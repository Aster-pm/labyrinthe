J = java
JC = javac
JD = javadoc
SRC = -sourcepath src -d
CL = -classpath
TEST = test4poo.jar test/game/

all : doc cls exe

doc : 
	$(JD) $(SRC) docs game

cls :
	$(JC) $(SRC) classes src/game/*.java
	

comptest :
	$(JC) $(CL) $(TEST)maze/*.java
	$(JC) $(CL) $(TEST)cell/*.java
	$(JC) $(CL) $(TEST)Item/*.java
	$(JC) $(CL) $(TEST)mazeAlgo/*.java

exe : cls
	$(J) $(CL) classes game.Game

exe_jeu: cls
	$(J) $(CL) classes game.jeu

exe_jeu_division: cls
	$(J) $(CL) classes game.jeu division

exe_jeu_recursif: cls
	$(J) $(CL) classes game.jeu recursif


jeu.jar : cls
	jar cvfm jeu.jar manifest.mf classes


exe_division : cls 
	$(J) $(CL) classes game.Game division

exe_recursif : cls
	$(J) $(CL) classes game.Game recursif


exeAlgo : cls
	$(J) $(CL) classes game.maze.maze

clean :
	rm -r classes
	rm -r docs