package game;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class GameDataReader {
    private String[] data;
    private int index;
    private String regex;

    public GameDataReader(String regex) {
        this.data = null;
        this.index = -1;
        this.regex = regex;

    }

    public GameDataReader() {
        this.data = null;
        this.index = -1;
        this.regex = "\n";
    }

    /**
     * Make a String table with a file splitted on the \n character
     * 
     * @param filename the name of the file must has .txt extension and with
     *                 different datas separated by \n
     * @throws Exception
     */
    public void setData(String filename) {
        this.index = -1;
        String read;
        try {
            File file = new File("./");
            String path = file.getAbsolutePath();
            path += "/data/";
            read = new String(Files.readAllBytes(Paths.get(path + filename)));

            this.data = read.split(this.regex);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String[] getData() {

        for (int it = 0; it < this.data.length; it++) {
            int index = (int) (Math.random() * this.data.length);
            String temp = this.data[it];
            this.data[it] = this.data[index];
            this.data[index] = temp;
        }
        return this.data;
    }

    public String[] getNormalData() {
        return this.data;
    }

    public String getOneData() {
        this.index = (this.index + 1) % this.data.length;

        return this.data[this.index];
    }
}
