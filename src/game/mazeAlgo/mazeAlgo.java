package game.mazeAlgo;

import java.util.Random;
import game.cell.Cell;
import game.maze.*;
import java.util.*;


public abstract class mazeAlgo {
    protected int width;
    protected int height;
    protected Maze theMaze;
    private final Random random = new Random();
  
  
  /**Creates a maze with the specified height and width, with all the walls filled in inside it.
  *@param h height of the maze
  *@param w width of the maze
  */
  public mazeAlgo(Maze theMaze){
    this.theMaze = theMaze;
    this.width = theMaze.getWidth();
    this.height = theMaze.getHeight();
  }

  /**Changes the value for the presence or absence of the wall of the cell specified within the parameters
  *@param x the height coordinate for the cell we want to modify
  *@param y the width coordinate for the cell we want to modify
  *@param w the wall to which the changes will apply (NORTH,EAST, SOUTH, WEST)
  *@param b boolean value for the presence or absence of the wall
  */
  public void setWall(int x, int y,String w, boolean b){
    this.theMaze.getCell(x, y).setWall(w, b);
  }

  public Maze getMaze(){
    return this.theMaze;
  }

  /**Fetches the cell with the chosen coordinates 
  *@param x height coordinate of the cell
  *@param y width coordinate of the cell
  *@return returns the cell
  */
  public Cell getCell(int x, int y){
    return this.theMaze.getCell(x, y);
  }
  
  /**
   * return the width
   * @return the width
   */
  public int getWidth() {
	  return this.width;
  }
  
  /**
   * return the height
   * @return the height
   */
  public int getHeight() {
	  return this.height;
  }

  /**
   * return a random boolean
   * @return boolean
   */
  public boolean getRandomBoolean(){
    return this.random.nextBoolean();
}

  //Méthodes pour simplifier le code de labyrinthe
  /**
   * set the last line 
   * @param y
   */
  public void setWallLastLine(int y){ //Pour gérer la derniere ligne (ne pas enlever les murs est)
    this.setWall(this.height-1, y, "e", false);
    this.setWall(this.height-1, this.width-1,"e", true);
}

  public void shiftWalls(int x, int y, Maze maze){}

  /**
   * return the neighbours cells if she is unvisited
   * @param cell the cell
   * @param maze the maze
   * @return unvisited neighbours
   */
  public ArrayList<Cell> getUnvisitedNeighbours(Cell cell, Maze maze){
      int x = cell.getX();
      int y = cell.getY();
      ArrayList<Cell> neighbours2 = new ArrayList<>();
      if(x+1 < maze.getWidth()){
          Cell neigbCell = maze.getCell(x+1, y);
          if(neigbCell.getVisited()==false){
              neighbours2.add(neigbCell);
          }
      }
      if(x-1 >= 0){
          Cell neigbCell = maze.getCell(x-1, y);
          if(neigbCell.getVisited()==false){
              neighbours2.add(neigbCell);

          }
      }
      if(y+1 < maze.getHeight()){
          Cell neigbCell = maze.getCell(x, y+1);
          if(neigbCell.getVisited()==false){
              neighbours2.add(neigbCell);
          }
      }
      if(y-1>=0){
          Cell neigbCell = maze.getCell(x,y-1);
          if(neigbCell.getVisited()==false){
              neighbours2.add(neigbCell);
          }
      }
      return neighbours2;
  }
}
