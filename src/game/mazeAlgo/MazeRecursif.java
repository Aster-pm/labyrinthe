package game.mazeAlgo;
import game.cell.Cell;
import game.maze.Maze;

import java.util.*;


public class MazeRecursif extends mazeAlgo{
    
    /**Creates a maze with the specified height and width, with all the walls filled in inside it.
    *@param h height of the maze
    *@param w width of the maze
    */
    public MazeRecursif(Maze theMaze) {
        super(theMaze);
    }

    /** Create a perfect maze
     * @param x the x coordinate of the first cell
     * @param y the y coordinate of the first cell
     * @param maze the maze with all the walls
     */
    public void shiftWalls(int x, int y, Maze maze) {
        Cell currentCell = maze.getCell(x, y);
        if (currentCell.getVisited()==false) {

            currentCell.setVisited(true);
            ArrayList<Cell> toVisit = getUnvisitedNeighbours(currentCell,maze);

            while (toVisit.size()>0) {
                
                int index = (int) Math.round(Math.random() * (toVisit.size()- 1));
                Cell choosenCell = toVisit.get(index);

                // chosenCell is on the right
                if (choosenCell.getX() - x == 1) { 
                    currentCell.setWall("s", false);
                    choosenCell.setWall("n", false);
                } 
                // choosenCell is on the left
                else if (choosenCell.getX() - x == -1) { 
                    currentCell.setWall("n", false);
                    choosenCell.setWall("s", false);
                } 
                // choosenCell is at the bottom
                else if (choosenCell.getY() - y == 1) { 
                    currentCell.setWall("e", false);
                    choosenCell.setWall("w", false);
                } 
                // choosenCell is on top
                else { 
                    currentCell.setWall("w", false);
                    choosenCell.setWall("e", false);
                }
                shiftWalls(choosenCell.getX(), choosenCell.getY(),maze);
                toVisit = getUnvisitedNeighbours(currentCell,maze);
            }
            
        }
    }

}
