package game.mazeAlgo;

import game.maze.Maze;

import java.util.*;

public class MazeDivision extends mazeAlgo {

    /**
     * Creates a maze with the specified height and width, with all the walls filled
     * in inside it.
     * 
     * @param h height of the maze
     * @param w width of the maze
     */
    public MazeDivision(Maze theMaze) {
        super(theMaze);
    }

    /**
     * do a maze
     * 
     * @param x
     * @param y
     * @param maze the maze
     */
    public void shiftWalls(int x, int y, Maze maze) {
        for (int i = 0; i < maze.getWidth(); i++) {
            maze.getCell(i, 0).setWall("w", true);
            maze.getCell(i, maze.getHeight() - 1).setWall("e", true);
        }
        for (int i = 0; i < maze.getHeight(); i++) {
            maze.getCell(0, i).setWall("n", true);
            maze.getCell(maze.getWidth() - 1, i).setWall("s", true);
        }

        Random seed = new Random();
        this.divise(0, maze.getWidth(), 0, maze.getHeight(), maze, seed);
    }

    /**
     * split the maze between Xstart Xend Ystart and Yend
     * 
     * @param Xstart the x coordinates start
     * @param Xend   the x coordinates end
     * @param Ystart the y coordinates start
     * @param Yend   the y coordinates end
     * @param maze   the maze to divide
     * @param seed
     */
    public void divise(int Xstart, int Xend, int Ystart, int Yend, Maze maze, Random seed) {

        if ((Xend - Xstart) > 1 && (Yend - Ystart) > 1) {
            int side;
            if ((Yend - Ystart) < (Xend - Xstart)) {
                side = 0;

            } else if ((Yend - Ystart) > (Xend - Xstart)) {
                side = 1;

            } else {
                side = seed.nextInt(2);
            }
            if (side == 1) {

                int wall = seed.nextInt((Yend - Ystart) - 1) + Ystart;
                // int wall = (int) (Math.random()*((Yend-Ystart)-1) + Ystart);

                // System.out.println(wall);

                for (int i = Xstart; i < Xend; i++) {
                    maze.getCell(i, wall).setWall("e", true);
                    maze.getCell(i, wall + 1).setWall("w", true);
                }

                this.makeRandomHorizontalHole(Xstart, Xend, wall, maze, seed);

                divise(Xstart, Xend, Ystart, wall + 1, maze, seed);

                divise(Xstart, Xend, wall + 1, Yend, maze, seed);

            } else {
                int wall = seed.nextInt((Xend - Xstart) - 1) + Xstart;
                // int wall = (int) (Math.random()*(Xend-Xstart-1) + Xstart);

                // System.out.println(wall);
                for (int j = Ystart; j < Yend; j++) {
                    maze.getCell(wall, j).setWall("s", true);
                    maze.getCell(wall + 1, j).setWall("n", true);
                }

                this.makeRandomVerticalHole(Ystart, Yend, wall, maze, seed);

                divise(Xstart, wall + 1, Ystart, Yend, maze, seed);

                divise(wall + 1, Xend, Ystart, Yend, maze, seed);
            }
        }

    }

    /**
     * do a random vertical hole
     * 
     * @param Ystart the y coordinate start
     * @param Yend   the y coordinate end
     * @param wall
     * @param maze   the maze
     * @param seed
     */
    public void makeRandomVerticalHole(int Ystart, int Yend, int wall, Maze maze, Random seed) {
        // System.out.println(Ystart);
        // System.out.println(Yend);

        int hole = seed.nextInt((Yend - Ystart)) + Ystart;

        // int hole = (int) (Math.random()*(Yend-Ystart-1) + Ystart);

        // System.out.println(hole);

        maze.getCell(wall, hole).setWall("s", false);
        maze.getCell(wall + 1, hole).setWall("n", false);

    }

    /**
     * do a random horizontal hole
     * 
     * @param Xstart the x coordinate start
     * @param Xend   the x coordinate end
     * @param wall
     * @param maze   the maze
     * @param seed
     */
    public void makeRandomHorizontalHole(int Xstart, int Xend, int wall, Maze maze, Random seed) {
        // System.out.println(Xstart);
        // System.out.println(Xend);

        int hole = seed.nextInt((Xend - Xstart)) + Xstart;

        // int hole = (int) (Math.random()*((Xend-Xstart)-1) + Xstart);

        // System.out.println(hole);
        maze.getCell(hole, wall).setWall("e", false);
        maze.getCell(hole, wall + 1).setWall("w", false);
    }
}