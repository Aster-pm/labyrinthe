package game.menu;

import java.io.Console;

public class MenuDisplayer {

    private static final String BG_RED = "\u001B[41m";
    private static final String RESET = "\u001B[0m";

    private String request;
    private String[][] choices;
    private String[] separator;
    private String[] color;
    private String input;

    /**
     * Create a MenuDisplayer object which the player can interact with
     * 
     * @param req the sentence to explain the situation for the player
     * @param cho the list of actions the player can do
     * @param in  the answer of the player, default null
     */
    public MenuDisplayer(String req, String[][] choices, String[] separators, String[] colors) {
        this.request = req;
        this.choices = choices;
        this.separator = separators;
        this.color = colors;
        this.input = "";
    }

    /**
     * @return the sentence to explain the situation for the player
     */
    public String getRequest() {
        return request;
    }

    /**
     * @return the list of actions the player can do
     */
    public String[][] getChoices() {
        return choices;
    }

    /**
     * @return the answer of the player
     */
    public int getInput() {
        int res = 0;
        try {
            res = Integer.parseInt(this.input);
        } catch (NumberFormatException e) {
            System.out.println("\t" + BG_RED + "! please enter an Integer !" + RESET);
            return res;
        }
        return res;
    }

    /**
     * @param request
     */
    public void setRequest(String request) {
        this.request = request;
    }

    /**
     * @param choices
     */
    public void setChoices(String[][] choices) {
        this.choices = choices;
    }

    /**
     * set the input on what the player has written in terminal
     */
    public void setInput() {
        // CREATE THE DISPLAY
        // The sentence
        String display = "\n" + this.request + "\n\n";
        // The Choices, rising all the [i] list with [i] seprator of [j] possibilities
        int key = 0;

        for (int i = 0; i < this.choices.length; i++) {

            if (this.choices[i].length > 0) {
                key++;
                display += "\t" + this.color[i] + key + " - " + this.choices[i][0];
            }

            for (int j = 1; j < this.choices[i].length; j++) {
                key++;
                display += this.color[i] + this.separator[i] + key + " - " + this.choices[i][j];
            }

            display += RESET;
            if (this.choices[i].length > 0) {
                display += "\n";
            }
        }
        Console console = System.console();
        this.input = console.readLine(display + RESET);
    }
}