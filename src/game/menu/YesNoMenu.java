package game.menu;

import java.io.Console;

public class YesNoMenu {

    private static final String BG_RED = "\u001B[41m";
    private static final String RESET = "\u001B[0m";
    
    private String question;
    private boolean input;

    public YesNoMenu(String question){
        this.question=question;
        this.input=false;
    }

    public String getSentence(){
        return this.question;
    }

    public boolean getInput(){
        return this.input;
    }

    public void setInput(){
        Console console = System.console();
        String display = "\t"+this.question+"\n\t1 - Yes   |   2 - No\n";
        String answer = console.readLine(display);
        int res = 0;
        try {
            res = Integer.parseInt(answer);
        } catch (NumberFormatException e) {
            System.out.println("\t"+BG_RED+"! please enter an Integer !"+RESET);
            res=0;
        }
        if(res==1){
            this.input=true;
        }
        else if(res==2){
            this.input=false;
        }
        else{
            System.out.println("\t"+BG_RED+"Your choice doesn't match with anything"+RESET);
            this.input=false;
        }
    }
}