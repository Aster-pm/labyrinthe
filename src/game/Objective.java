package game;

import game.character.Hero;
import game.item.Item;
import game.item.Jewel;
import game.item.Relic;
import game.maze.Maze;
import game.action.Action;
import game.cell.Cell;
import java.lang.Math;
import java.util.HashMap;
import java.util.Random;

public class Objective {

    private Item[] items;
    private Maze maze;
    private Cell cell;
    private int gold;
    private HashMap<Class, Boolean> mandatory;
    private String[] possiblesClues;
    private int givenClues;
    private String[] fakeClues;

    public Objective(Cell c, int gold, Action[] acts, Item[] items, Maze maze) {
        int actsLength = 0;
        this.maze = maze;
        this.cell = c;
        this.gold = gold;
        this.mandatory = new HashMap<Class, Boolean>();
        this.items = items;
        if (acts != null) {
            for (Action act : acts) {
                this.mandatory.put(act.getClass(), false);
            }
            actsLength = acts.length;
        }

        int clues = this.gold == 0 ? 3 : 4; // 3 clues a donner si il n'y a pas d'or a avoir pour la fin, 4 clues sinn
        // System.out.println(clues);
        this.fakeClues = new String[64];
        this.possiblesClues = new String[clues + actsLength + items.length];
        this.givenClues = 0;
        this.initClues();
    }

    public Boolean getMandatory(Action act) throws Exception {
        if (this.mandatory.containsKey(act)) {
            return this.mandatory.get(act);
        } else {
            throw new Exception("ya pas cette action");
        }
    }

    public HashMap<Class, Boolean> getMandatory() {
        return this.mandatory;
    }

    public Item[] getItems() {
        return this.items;
    }

    public Item getItems(int i) {
        return this.items[i];
    }

    public Cell getCell() {
        return this.cell;
    }

    public int getGold() {
        return this.gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public void setMandatory(Action act) {
        try {
            getMandatory(act);
            this.mandatory.put(act.getClass(), true);
            return;
        } catch (Exception e) {
            return;
        }
    }

    public void initClues() {
        int i;
        i = 0;
        this.possiblesClues[i] = this.CluePosition();
        i++;

        this.possiblesClues[i] = this.ClueDistance();
        i++;

        this.possiblesClues[i] = this.ClueDirection();
        i++;

        if (this.gold != 0) {
            this.possiblesClues[i] = this.ClueGold();
            // System.out.println("gold : " + this.ClueGold());

            i++;
        }

        for (Class cl : this.mandatory.keySet()) {
            if (cl != null) {
                this.possiblesClues[i] = cl.getSimpleName();
                // System.out.println("act : " + act.getName());

                i++;
            }

        }

        if (this.items != null && this.items[0] != null && this.items.length > 0) {
            this.possiblesClues[i] = this.ClueItems();
            // System.out.println("item for clue : " + this.ClueItems());
            i++;
        }

        double rndfake = Math.random();
        for (int y = 0; y < rndfake * this.possiblesClues.length + 1; y++) {
            this.fakeClues[y] = this.generateFakeClue();
        }

        // copié ici la juste la enfait ici la sur ce site ici sur internet, sur le web,
        // la toile en ligne online
        // https://iqcode.com/code/java/how-to-randomize-an-array-java
        // randomize les indices
        for (int it = 0; it < this.possiblesClues.length; it++) {
            int index = (int) (Math.random() * this.possiblesClues.length);
            String temp = this.possiblesClues[it];
            this.possiblesClues[it] = this.possiblesClues[index];
            this.possiblesClues[index] = temp;
        }

        // parcours pour enlever null
        int nullClue = this.possiblesClues.length - 1;
        for (int clue = 0; clue < nullClue; clue++) {
            if (this.possiblesClues[clue] == null) {
                while (this.possiblesClues[nullClue] == null && nullClue > clue) {
                    nullClue -= 1;
                }
                this.possiblesClues[clue] = this.possiblesClues[nullClue];
                this.possiblesClues[nullClue] = null;

            }
            // System.out.println("Test Clue dans le randomize -- clue n°"+clue+" :
            // ________" + this.possiblesClues[clue]);

        }

        // faut penser à se débarasser des null
    }

    // Toutes les fonctions pour créer des indices

    public String giveRandomClue() {

        // renvoies le premier indices qui n'a pas déjà été donné aux joueurs, si tout
        // les indices ont été donné, en revoies un aléatoirement;
        String rep = null;
        Random rng = new Random();
        rep = this.possiblesClues[this.givenClues];
        if (rep != null) {
            this.givenClues++;
        }

        while (rep == null) {
            rep = this.possiblesClues[rng.nextInt(this.givenClues + 1)];
        }

        // for(String str: this.possiblesClues){
        // System.out.println(str+"\n");
        // }

        // System.out.println("test random ----" + rep + "\n");

        return rep;

    }

    public String giveFakeRandomClue() {
        String rep = null;
        while (rep == null) {
            rep = this.fakeClues[(int) (Math.random() * this.fakeClues.length)];
        }
        return rep;

    }

    public String ClueItems() {
        int nbJewel = 0, nbNFT = 0;

        String rep = "gather ";
        for (Item it : this.items) {
            if (it instanceof Jewel) {
                nbJewel++;
            } else if (it instanceof Relic) {
                nbNFT++;
            }
        }
        if (nbJewel > 0) {
            rep += nbJewel + " Jewel ";
            if (nbNFT > 0) {
                rep += " and ";
            }
        }
        if (nbJewel > 0) {
            rep += nbNFT + " Relic ";
        }
        return rep + " to open the Trapdoor";
    }

    public String ClueDistance() {
        Cell distantCell = this.cell;

        // On prends une cellule au hasard en étant sur que c'est pas la cellule de
        // victoire
        while (distantCell == this.cell) {
            int x, y;
            y = (int) (Math.random() * this.maze.getWidth());
            x = (int) (Math.random() * this.maze.getHeight());

            distantCell = this.maze.getCell(x, y);
        }

        // Ici ça pythagore en légende mais plus trop enfaite
        double dist = Math.round((Math.sqrt(
                Math.pow(this.cell.getX() - distantCell.getX(), 2) +
                        Math.pow(this.cell.getY() - distantCell.getY(), 2)))
                * 100);
        dist = dist / 100;

        dist = Math.abs(this.cell.getX() - distantCell.getX()) + Math.abs(this.cell.getY() - distantCell.getY());
        return "reach the room " + dist + " rooms away from [" + distantCell.getX() + ";" + distantCell.getY()
                + "]. At least if you can walk through walls...";
    }

    public String ClueDirection() {
        Cell distantCell = this.cell;
        // Find relative direction between 2 cells
        while (distantCell == this.cell) {
            int x, y;
            y = (int) (Math.random() * this.maze.getWidth());
            x = (int) (Math.random() * this.maze.getHeight());
            distantCell = this.maze.getCell(x, y);
        }
        String direction = "";
        // North
        if (this.cell.getX() < distantCell.getX()) {
            direction = "North ";
        }
        // South
        else if (this.cell.getX() > distantCell.getX()) {
            direction = "South ";
        }
        // East
        if (this.cell.getY() > distantCell.getY()) {
            direction += "East ";
        }
        // West
        if (this.cell.getY() < distantCell.getY()) {
            direction += "West ";
        }
        return "reach a room which is on the " + direction + "from the room ["
                + distantCell.getX() + ";"
                + distantCell.getY() + "]";
    }

    public String ClueGold() {
        return "gather " + this.gold + "G";
    }

    public String CluePosition() {
        return "reach the room [" + this.cell.getX() + ";" + this.cell.getY() + "]";
    }

    public boolean checkObjectives(Hero player) {

        if (player.getCell() != this.cell) {
            return false;
        }

        System.out.println("You see a trapdoor on the floor in the middle of the room");

        if (player.getGold() < this.gold) {
            // pas assez door ouh ouh le chialeur bebe cadum
            System.out.println("... but you have to bring much more gold to open it");
            return false;
        }
        /*
         * for (Boolean b : this.mandatory.values()) {
         * if (!b) {
         * return false;
         * }
         * }
         */

        // System.out.println("player cell : "+ player.getCell().getX() +
        // player.getCell().getY() + " pour : " +this.cell.getX() + this.cell.getY());

        int nbJewel = 0, nbNFT = 0, objectiveJewel = 0, objectiveNFT = 0;
        for (Item it : player.getInventory()) {
            if (it instanceof Jewel) {
                nbJewel++;
            }
            if (it instanceof Relic) {
                nbNFT++;
            }
        }
        for (Item it : this.items) {
            if (it instanceof Jewel) {
                objectiveJewel++;
            } else if (it instanceof Relic) {
                objectiveNFT++;
            }
        }
        if (nbJewel < objectiveJewel || nbNFT < objectiveNFT) {
            // pas les items nécessaires
            System.out.println("... but you have to bring some specifics items to open it");
            return false;
        }

        return true;
    }

    public String generateFakeClue() {

        switch ((int) (Math.random() * 3)) {
            case 0:
                return FakeClueDistance();
            case 1:
                return FakeClueGold();
            case 2:
                return FakeCluePosition();

        }
        return null;

    }

    public String FakeClueDistance() {
        Cell distantCell;

        int x, y;
        y = (int) (Math.random() * this.maze.getWidth());
        x = (int) (Math.random() * this.maze.getHeight());

        distantCell = this.maze.getCell(x, y);
        return "reach the cell wich is " + (int) (Math.random() * 100) + " meters away from the room ["
                + distantCell.getX()
                + ";"
                + distantCell.getY() + "]";

    }

    public String FakeClueGold() {
        // c'est juste histoire de retourner n'importe quoi
        return "gather " + (int) (Math.random() * (this.gold + 50) * 2 - 250)
                + "G";
    }

    public String FakeCluePosition() {
        return "reach the room [" + (int) (Math.random() * this.maze.getHeight())
                + ";"
                + (int) (Math.random() * this.maze.getWidth()) + "]";
    }

}