package game.character;
import game.action.*;
import game.cell.Cell;


public abstract class GameCharacter {
	protected String name;
	protected Cell location;
	protected Action[] actions;

	/** Create a new character
	 * @param name the name of the character
	 * @param theCell the Cell where the character is
	 * @param actions a list of actions available for the character
	 */
	public GameCharacter(String name, Cell theCell, Action[] actions) {
		this.name = name;
		this.location = theCell;
		this.actions = actions;
	}

	/** Return character's name
	 * @return String the character's name
	 */
	public String getName(){
		return this.name;
	}

	/** Return character's Cell
	 * @return Cell the Cell where the character is
	 */
	public Cell getCell(){
		return this.location;
	}

	/** Modify character's Cell
	 * @return Cell the Cell where the character will go
	 */
	public void setCell(Cell newCell){
		this.location = newCell;
	}

	/** Return character's Actions
	 * @return Action[] the list of actions available for the character
	 */
	public Action[] getActions(){
		return this.actions;
	}
	
	/** Do the action which corresponds to the number
	 * @param action int the action's number
	 * @throws ActionErrorException
	 * @throws ErrorToken
	 */
	public abstract void doAction(int action);
}
