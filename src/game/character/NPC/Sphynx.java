package game.character.NPC;
import game.action.*;
import game.cell.*;
import game.item.Parchment;

public class Sphynx extends NPC{

  private String[] enigma;
  private Parchment reward;
    
  /** Create a new Sphynx
	 * @param enigmas is the question which he can ask
	 */
  public Sphynx(String name, Cell location, Action[] actions, String[] enigmas, Parchment reward){
    super(name, location, actions);
    this.enigma = enigmas;
    this.reward = reward;
}

  /** Return the Sphynx's question
   * @return String the Sphynx's question
   */
  public String[] getEnigma(){
    return this.enigma;
  }

  /** Set the Sphynx's question
   * @param newEnigma the Sphynx's question
   */
  public void setEnigma(String[] newEnigma){
    this.enigma = newEnigma;
  }

  /** Return the Sphynx's reward
   * @return Parchment the Sphynx's reward
   */
  public Parchment getReward(){
    return this.reward;
  }

  /** Set the Sphynx's reward
   * @param newParchment the Sphynx's reward
   */
  public void setReward(Parchment newParchment){
    this.reward = newParchment;
  }

  /** Do the action chosen by an int
   * @param act the int to choose an action
   * @throws ActionErrorException if the number doesn't match with an action
   * @throws ErrorToken
   */
  public void doAction(int act){
        this.actions[act].launch(this);
  }
}