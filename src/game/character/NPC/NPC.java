package game.character.NPC;
import game.action.*;
import game.cell.Cell;
import game.character.GameCharacter;

public abstract class NPC extends GameCharacter{
  // protected String clue;

  /** Create a new NPC
   * @param name the NPC's name
   * @param location the NPC's Cell
   * @param actions a list of actions available for the NPC
   */
  public NPC(String name, Cell location, Action[] actions){
    super(name, location, actions);
  }

  /** Do the action which corresponds to the number
	 * @param action int the action's number
   * @throws ActionErrorException
 * @throws ErrorToken
	 */
	public abstract void doAction(int action);


  /**
   * return the number of actions
   * @return int
   */
  public int nbActions(){
    return actions.length;
  }

  /**
   * return the i-eme action
   * @param i the index of the action
   * @return Action
   */
  public Action getAction(int i){
    return actions[i];
  }
}
