package game.character.NPC;
import game.action.*;
import game.cell.*;
import game.item.Parchment;

public class Explorer extends NPC{

  private boolean crazy;
  private String clue;
    
  /** Create a new Explorer
	 * @param crazy if the Walker is crazy
	 */
  public Explorer(String name, Cell location, Action[] actions, Boolean crazy){
    super(name, location, actions);
    this.crazy = crazy;
    this.clue = null;
}

  /** Return the Explorer's mental health
   * @return Crazy if the Walker is crazy
   */
  public boolean getCrazy(){
    return this.crazy;
  }

  /** Set the Explorer's mental health
   * @param crazy if the Walker is crazy
   */
  public void setCrazy(Boolean crazy){
    this.crazy = crazy;
  }

  /** 
   * @return the Explorer's clue
   */
  public String getClue() {
    return this.clue;
  }

  /** Set the Explorer's clue
   * @param newClue the new clue to set
   */
  public void setClue(String newClue) {
    this.clue = newClue;
  }

    /** Do the action chosen by an int
   * @param act the int to choose an action
   * @throws ActionErrorException if the number doesn't match with an action
   */
  public void doAction(int act){
    if(act < actions.length){
        this.actions[act].launch(this);
    }
  }
}