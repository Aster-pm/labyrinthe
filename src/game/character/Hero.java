package game.character;
import game.action.*;
import game.cell.*;
import game.item.*;

public class Hero extends GameCharacter{
    private int gold;
    private Item[] inventory;
    private int turn;

    /** Create a new Hero who's the player.
     * his inventory can take up to 10 items.
	 * @param pseudo the pseudo of the player
	 */
    public Hero(String name, Cell location, Action[] actions, int gold, Item[] inventory){
        super(name, location, actions);
        this.gold = gold;
        this.location.SetHero(this);
        this.inventory = inventory;
        this.turn = 0;
    }

    /** Return player's money
	 * @return int the player's money
	 */
	public int getGold(){
		return this.gold;
	}

    /**
     * return the number of turn
     * @return int
     */
    public int getTurn(){
        return this.turn;
    }

    /** Return player's Item at index i of his inventory
	 * @return Item one of the player's items
	 */
	public Item getItem(int i){
		return this.inventory[i];
	}

    /** Return player's Inventory
	 * @return Item[] one of the player's items
	 */
	public Item[] getInventory(){
		return this.inventory;
	}

    /**
     * set a new inventory en destroy the old inventory
     * @param its the new inventory
     */
    public void setInventory(Item[] its){
        for(int i = 0;i< this.inventory.length;i++){
            this.inventory[i] = its[i];
        }
        return;
    }

    /**
     * set the param turn at i
     * @param i the new value of the param turn
     */
    public void setTurn(int i){
        this.turn = i;
    }

    /**
     * add i at the param turn
     * @param i the add value of the param turn
     */
    public void addTurn(int i){
        this.turn += i;
    }

    /** Add gold into player's money
     * @param i the amount of gold to add
     */
    public void addGold(int i){
        this.gold += i;
    }

    /** Take gold from player's money
     * @param i the amount of gold to take
     */
    public void takeGold(int i){
        this.gold = this.gold - i;
    }

    /** Do the action chosen by an int
     * @param act the int to choose an action
     * @throws ActionErrorException if the number doesn't match with an action
     */
    public void doAction(int act){
            this.actions[act].launch(this);
    }

    /**
     * remove the item
     * @param item the item
     */
    public void removeItem(Item item){
        for(int i=0;i<this.inventory.length;i++){
            if(this.inventory[i].equals(item)){
                inventory[i] = null;
                break;
            }
        }
        for(int i=0;i<this.inventory.length-1;i++){
            if(this.inventory[i]==null){
                Item next = this.inventory[i+1];
                this.inventory[i]=next;
                this.inventory[i+1]=null;
            }
        }
    }

    /**
     * Takes the hero actual inventory and add the item in parameter in the right place so there's no gap left inbetween each item of the inventory
     * @param it the item inserted into the inventory
     * @return the new state of the inventory
     */
    public Item[] addItem(Item it){

        for(int i = 0;i<this.inventory.length;i++){
            if(this.inventory[i]==null){
                this.inventory[i] = it;
                break;
            }
        }
        return this.inventory;
    }
}