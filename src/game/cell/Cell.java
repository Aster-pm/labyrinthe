package game.cell;
import java.util.*;

import game.item.Item;
import game.character.*;
import game.character.NPC.*;


public class Cell{
	private boolean visited;
	private int x;
	private int y;
	private HashMap<String,Boolean> walls;
	private Item[] item;
	private NPC[] npc;
	private Hero player;

	/**
	 * Initialize the Cell object with an empty hashmap for walls, then calls initWalls() which fills the map with each walls set to true
	 * @param x it's x coordinate
	 * @param y it's y coordinate
	 * @param withWalls the state of the walls on initialization, false means no wall, true means wall on every direction.
	 */
	public Cell(int x,int y,boolean withWalls){
		this.visited = false;
		this.x = x;
		this.y = y;
		this.walls = new HashMap<>();
		this.item = new Item[64];
		this.npc = new NPC[20];
		this.player = null;
		if(withWalls == true){
			initWalls(withWalls);
		}
	}

	/**
	 * Init the walls to withWalls value for each of them. Only used by constructor
	 * @param withWalls the sate of the walls for their initialization, false means no wall, true means wall on every direction.
	 */
	private void initWalls(boolean withWalls){
		String[] directions = new String[]{"s","n","w","e"};
		for(String d : directions){
			this.walls.put(d, withWalls);
		}
	}

	/**
	 * Return a coordinate of the cell
	 * @return int the x coordinate
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * Return a coordinate of the cell
	 * @return int the y coordinate
	 */
	public int getY() {
		return this.y;
	}

	/**
	 * If the cell is empty return true, return false else
	 * @return true if the cell is empty, false else
	 */
	public boolean isEmpty(){
		if (this.item[0]== null){
			return true;
		}
		return false;
	}
	
	/**
	 * Return true if the cell has already been visited by the player, else false
	 * @return boolean
	 */
	public boolean getVisited() {
		return this.visited;
	}

	/**
	 * Change the state of visited attribute
	 * @param state boolean true if the cell has already been visited by the player, else false
	 */
	public void setVisited(boolean state) {
		this.visited = state;
	}

	/**
	 * return the player
	 * @return the player
	 */
	public Hero getHero(){
		return this.player;
	}

	/**
	 * set the param character in the cell if the character is an hero
	 * @param character the player
	 */
	public void SetHero(Hero character){
		if(character instanceof Hero){
		this.player = character;
		}
	}

	/**
	 * set null for this param player 
	 */
	public void removeHero(){
		this.player = null;
	}

	/** Return the NPC in this cell
	 * @return NPC
	 */
	public NPC[] getNPC() {
		return this.npc;
	}

	/** Return the NPC at index i in this cell
	 * @return NPC
	 */
	public NPC getOneNPC(NPC npc) {
		NPC res=null;
		for(int i=0;i<this.npc.length;i++){
			if(npc.equals(this.npc[i])){
				res = this.npc[i];
			}
		}
		return res;
	}

	/** Set a new NPC at the end of the list
	 * @param newNPC the new NPC
	 */
	public void addNPC(NPC newNPC) {
		int i = 0;
		while(this.npc[i]!=null){
			i+=1;
		}
		this.npc[i]=newNPC;
	}

	/** remove the current NPC and decay the rest of them to note have
	 * empty cell in the middle of the list
	 * @param i the index of the removed NPC
	 */
	public NPC removeNPC(NPC npc){
		for(int i=0;i<this.npc.length;i++){
			if(npc.equals(this.npc[i])){
				this.npc[i]=null;
			}
		}
		for(int i=0;i<this.npc.length-1;i++){
			if(this.npc[i]==null){
				this.npc[i]=this.npc[i+1];
				this.npc[i+1]=null;
			}
		}
		return npc;
	}
	
	/**
	 * Return the list of all items contained into the Cell
	 * @return Item[] the items
	 */
	public Item[] getItem() {
		return this.item;
	}

	/**
	 * return the item contained into the Cell at the param i place
	 * @param i the index of the item
	 * @return Item
	 */
	public Item getOneItem(int i){
		return this.item[i];
	}

	/** Put a new Item at the end of the list
	 * @param newItem
	 */
	public void addItem(Item newItem){
		int i = 0;
		while(this.item[i]!=null){
			i+=1;
		}
		this.item[i]=newItem;
		newItem.setCell(this);
	}

	public int nbOfWall(){
		int rep= 0;
		Collection<Boolean> coll = this.walls.values();
		for(Boolean bl : coll){
			if(bl){
				rep+=1;
			}
		}
		return rep;
	}

	/**
	 * Remove the item at i index, fill the void in the tab and keep the order of items
	 * @param i
	 * @return the item 
	 */
    public Item removeItem(int i) {
		Item res = this.item[i];
		this.item[i] = null;
		while(this.item[i]==null && i<19){
			this.item[i]=this.item[i+1];
			this.item[i+1]=null;
			i+=1;
		}
		return res;
	}

	/**
     *returns a boolean whether two cells are equal
     *@param o the object that we will compare to
     *@return true if the two cells are equal else false
    */
    public boolean equals(Object o){
        if (o instanceof Cell){
            Cell other = (Cell) o;
            return (this.x == other.x) && (this.y == other.y) && (this.walls==other.getWalls());
        }
        else
            return false;
        }

	/**
	 * Return information on the cell's walls (direction north, south, east or west and true if there is a wall, else false)
	 * @return HashMap<String,Boolean> the four walls
	 */
	public HashMap<String,Boolean> getWalls() {
		return this.walls;
	}

	/**
	 * Check if the wall in d direction is set to true (there is a wall) or false (there's no wall)
	 * @param d the direction to check
	 * @return the satus of the wall, false tehre's no wall, true there's a wall.
	 */
	public boolean checkWall(String d){
		if(this.walls.get(d) != null){
			return this.walls.get(d);
		}
		else{
			return false;
		}
	}

	/**
	 * Set or unset a wall on the choosen direction
	 * @param direction String the direction(north, south, east or west)
	 * @param value boolean true for a wall, else false
	 */
	public void setWall(String direction,Boolean value) {
		if(this.walls.containsKey(direction)){
			this.walls.remove(direction);
		}
		this.walls.put(direction, value);
	}
}