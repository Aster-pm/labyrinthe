package game.maze;

import java.util.Random;

import game.cell.Cell;
import game.mazeAlgo.mazeAlgo;

public class Maze {

	private static final String BLACK = "\033[0;30m";
	private static final String RED = "\033[0;31m";
	private static final String BG_GREEN = "\033[0;42m";
	private static final String YELLOW = "\033[0;33m";
	private static final String BLUE = "\033[0;34m";
	private static final String PURPLE = "\033[0;35m";
	private static final String CYAN = "\033[0;36m";
	private static final String WHITE = "\033[0;37m";
	private static final String RESET = "\u001B[0m";

	private Cell[][] cells;
	private int width;
	private int height;

	/**
	 * Initialize a new Maze
	 * 
	 * @param width  The width of the maze
	 * @param height The height of the maze
	 * @param walls  True -> There is a wall on every cell. False -> There is no
	 *               wall at all
	 */
	public Maze(int height, int width, boolean walls) {
		this.width = width;
		this.height = height;
		this.cells = new Cell[width][height];

		for (int y = 0; y < this.height; y++) {
			for (int x = 0; x < this.width; x++) {
				this.cells[x][y] = new Cell(x, y, walls);
			}
		}
	}

	/**
	 * Return the cells of this maze
	 * 
	 * @return Cell[][]
	 */
	public Cell[][] getCells() {
		return this.cells;
	}

	/**
	 * Return the cell at x y coordinates
	 * 
	 * @param x The x coordinate
	 * @param y The y coordinate
	 * @return Cell
	 */
	public Cell getCell(int x, int y) {
		return this.cells[x][y];
	}

	/**
	 * Return the width of this maze
	 * 
	 * @return int
	 */
	public int getWidth() {
		return this.width;
	}

	/**
	 * Return the height of this maze
	 * 
	 * @return
	 */
	public int getHeight() {
		return this.height;
	}

	/**
	 * Create a perfect maze
	 * 
	 * @param x    the x coordinate of the first cell
	 * @param y    the y coordinate of the first cell
	 * @param maze the maze with all the walls
	 */
	public void initBoard(mazeAlgo algo) {
		algo.shiftWalls(this.width - 1, this.height - 1, this);
		for (int i = 0; i < this.width; i++) {
			for (int j = 0; j < this.height; j++) {
				this.getCell(i, j).setVisited(false);
			}
		}
	}

	public Cell getRandomCell() {
		Random rng = new Random();

		return this.getCell(rng.nextInt(this.width), rng.nextInt(this.height));
	}

	/**
	 * Display this maze in the terminal
	 */
	public void displayMaze() {
		String topLine;
		String middleLine;
		String bottomLine = "";
		Cell currentCell;
		// Créer le Header
		String headerTop = "        ";
		for (int i = 0; i < this.width; i++) {
			if(i>9){
				headerTop += "[" + i + "]";
			}
			else{
				headerTop += " [" + i + "]";
			}
		}
		System.out.println(headerTop+"\n");
		// Pour chaque ligne
		for (int x = 0; x < width; x++) {
			topLine = "        ";
			if(x>9){
				middleLine = " [" + x + "]   ";
			}
			else{
				middleLine = " [" + x + "]    ";
			}
			bottomLine = "        ";
			// Pour chaque Cell
			for (int y = 0; y < height; y++) {
				currentCell = this.getCell(x, y);
				// check du mur Nord
				if (currentCell.checkWall("n")) {
					topLine += "+---";
				} else {
					topLine += "+   ";
				}
				// check du mur Ouest
				if (currentCell.checkWall("w")) {
					middleLine += "|";
				} else {
					middleLine += " ";
				}
				// check du Héros
				if (currentCell.getHero() != null) {
					String initial = "" + currentCell.getHero().getName().charAt(0);
					middleLine += BG_GREEN+" "+initial.toUpperCase()+" "+RESET;
				} else if (currentCell.getVisited()) {
					middleLine += "   ";
				} else {
					middleLine += " . ";
				}
				// check pour la dernière colonne
				if (currentCell.getY() == this.height - 1) {
					topLine += "+";
					middleLine += "|";
				} else {
					middleLine += "";
				}
				// check pour la dernière ligne
				if (currentCell.getX() == this.width - 1) {
					bottomLine += "+---";
				} else {
					bottomLine += "";
				}
			}
			System.out.println(topLine);
			System.out.println(middleLine);
		}
		// dernière ligne
		System.out.println(bottomLine + "+");
	}

	/**
	 * debug and test utility tool ======
	 * 
	 * @param o the object we want to compare
	 * @return True if the maze has exactly the same Cell[], return false if not or
	 *         if the the object o is not a maze type
	 */
	public boolean equals(Object o) {
		if (o instanceof Maze) {
			Maze other = (Maze) o;
			for (int i = 0; i < other.getHeight(); i++) {
				for (int j = 0; i < other.getWidth(); j++) {
					if (this.getCells()[i][j] != other.getCells()[i][j]) {
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}
}