package game.action;

import game.character.GameCharacter;
import game.character.Hero;
import game.character.NPC.Trader;
import game.item.Item;
import game.maze.Maze;
import game.menu.MenuDisplayer;
import game.menu.YesNoMenu;

public class SellMenu extends Action {
    private Item[] inventory;
    private int nbItems;
    private Trader trader;
    private Hero player;

    /**
     * Create the action with which the player can sell
     * an Item to the trader who launched this action
     * 
     * @param maze
     */
    public SellMenu(Maze maze) {
        super(maze);
        this.name = "SELL AN ITEM";
        this.done = false;
    }

    /**
     * The player choose an Item to sell and
     * the method modify the game depending on player's answer
     */
    public void launch(GameCharacter c) {
        // Print action header
        System.out.println(LINE + this.name + "\n");

        // Initialisation of the menu
        createKeys(c);
        MenuDisplayer menu = setMenu();
        menu.setInput();
        int answer = menu.getInput();
        System.out.println("\n");

        // Sell the object
        if (answer > 0 && answer < this.nbItems + 1) {
            Item theItem = this.inventory[answer - 1];

            // Trader's inventory is full
            System.out.println("TEST__");
            System.out.println(count(this.trader.getInventory()));
            System.out.println(this.trader.getInventory().length);
            if (count(this.trader.getInventory()) == this.trader.getInventory().length) {
                System.out.println(BG_CYAN + this.trader.getName() + " has no more place in his inventory" + RESET);
            }

            // Trader has not enough money
            else if (theItem.getsellValue() > this.trader.getGold()) {
                System.out.println(BG_CYAN + this.trader.getName() + " doesn't have enough money to buy "
                        + theItem.getName() + RESET);

            }

            else {
                // Confirmation
                String conf = GREEN + "Are you sure that you want to sell " + theItem.getName() + " for " + YELLOW
                        + theItem.getsellValue() + "G" + GREEN + " ?" + RESET;
                YesNoMenu confirmation = new YesNoMenu(conf);
                confirmation.setInput();
                System.out.println("\n");

                if (confirmation.getInput()) {
                    // Move the item and earn money
                    this.done = true;
                    this.player.removeItem(theItem);
                    this.player.addGold(theItem.getsellValue());
                    this.trader.addItem(theItem);
                    this.trader.takeGold(theItem.getsellValue());
                    theItem.setPickedUp(true);
                    System.out.println(BG_GREEN + "You sold " + theItem.getName() + " for " + BG_YELLOW
                            + theItem.getsellValue() + "G" + RESET);
                }
            }

        }
        // Back
        else if (answer == this.nbItems + 1) {
            return;
        } else {
            printError();
        }
        this.launch(c);
        return;
    }

    /**
     * Initialize the attributes
     */
    public void createKeys(GameCharacter c) {
        // Get the Characters
        this.trader = (Trader) c;
        this.player = this.trader.getCell().getHero();
        // Initialize attributes
        this.inventory = player.getInventory();
        this.nbItems = count(this.inventory);
    }

    /**
     * Create the menu when you want to sell something to the Trader
     * You can only choose an Item from your inventory
     * 
     * @return the menu which the launch() method will use
     */
    public MenuDisplayer setMenu() {

        String request = GREEN + "What do you want to sell ?\n" + RESET;
        request += GREEN + "You have " + YELLOW + this.player.getGold() + " gold\n" + RESET;
        request += CYAN + this.trader.getName() + " has " + YELLOW + this.trader.getGold() + " gold\n" + RESET;

        // Empty Inventory player
        if (count(this.inventory) == 0) {
            request += GREEN + "\nYour inventory is empty..." + RESET;
        }

        // Create Item lists
        String inventoryName[] = new String[this.nbItems];
        Item theItem;
        for (int i = 0; i < this.nbItems; i++) {
            theItem = this.inventory[i];
            inventoryName[i] = theItem.getName() + " : " + theItem.getsellValue();
        }

        // Create Back option
        String back[] = new String[1];
        back[0] = "Back";

        // Create choices list
        String choices[][] = new String[2][];
        choices[0] = inventoryName;
        choices[1] = back;

        // Create separators
        String separators[] = new String[2];
        separators[0] = "\n\t";
        separators[1] = "";

        // Create color list
        String colors[] = new String[2];
        colors[0] = GREEN;
        colors[1] = RESET;
        MenuDisplayer menu = new MenuDisplayer(request, choices, separators, colors);
        return menu;
    }

}
