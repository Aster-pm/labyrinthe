package game.action;

import java.util.Random;

import game.cell.Cell;
import game.character.GameCharacter;
import game.character.NPC.NPC;
import game.maze.Maze;
import game.menu.MenuDisplayer;

public class MoveNPC extends Action {

    private Random random;
    private NPC npc;
    private Cell currentCell;

    public MoveNPC(Maze maze) {
        super(maze);
        this.random = new Random();
    }

    public void move(Cell destination) {
        this.currentCell.removeNPC((this.npc));
        this.npc.setCell(destination);
        destination.addNPC(this.npc);
    }

    public void launch(GameCharacter c) {
        // Initialisation of attributes
        this.createKeys(c);

        int x = this.currentCell.getX();
        int y = this.currentCell.getY();
        Cell destinationCell;
        int direction = this.random.nextInt(4);
        int attempt = 0;
        // NPC tries to move in the 4th direction, don't move if there's a wall or too
        // much NPCs in the cell
        while (attempt < 4) {
            // North
            if (direction == 0 && !this.currentCell.checkWall("n") && this.currentCell.getX() - 1 >= 0) {
                destinationCell = this.maze.getCell(x - 1, y);
                // count if the cell is full
                if (count(destinationCell.getNPC()) < destinationCell.getNPC().length) {
                    move(destinationCell);
                    return;
                }
            }
            // South
            if (direction == 1 && !this.currentCell.checkWall("s")
                    && (this.currentCell.getX() + 1 < this.maze.getWidth())) {
                destinationCell = this.maze.getCell(x + 1, y);
                // count if the cell is full
                if (count(destinationCell.getNPC()) < destinationCell.getNPC().length) {
                    move(destinationCell);
                    return;
                }
            }
            // East
            if (direction == 2 && !this.currentCell.checkWall("e")
                    && this.currentCell.getY() + 1 < this.maze.getHeight()) {
                destinationCell = this.maze.getCell(x, y + 1);
                // count if the cell is full
                if (count(destinationCell.getNPC()) < destinationCell.getNPC().length) {
                    move(destinationCell);
                    return;
                }
            }
            // West
            if (direction == 3 && !this.currentCell.checkWall("w")
                    && this.currentCell.getY() - 1 >= 0) {
                destinationCell = this.maze.getCell(x, y - 1);
                // count if the cell is full
                if (count(destinationCell.getNPC()) < destinationCell.getNPC().length) {
                    move(destinationCell);
                    return;
                }
            }
            // if he didn't succeed to move, he try the newt direction
            attempt += 1;
            direction = (direction + 1) % 4;
        }
    }

    public void createKeys(GameCharacter c) {
        // Cast of the NPC
        this.npc = (NPC) c;
        this.currentCell = npc.getCell();
    }

    public MenuDisplayer setMenu() {
        return null;
    }
}
