package game.action;

import game.character.GameCharacter;
import game.character.Hero;
import game.character.NPC.Trader;
import game.maze.Maze;
import game.menu.MenuDisplayer;

public class TradeWith extends Action {
    private Trader trader;
    private Hero player;

    /**
     * Create the action with wich the player can buy or sell
     * something to the trader
     * 
     * @param maze
     */
    public TradeWith(Maze maze) {
        super(maze);
        this.name = "TALK TO A TRADER";
        this.done = false;
    }

    /**
     * The player choose between selling or buying an item and
     * the method modify the game depending on player's answer
     */
    public void launch(GameCharacter c) {
        // Initialisation of attributes
        createKeys(c);

        // Print header of the action
        System.out.println(LINE + this.name);

        // Initialisation of the menu
        MenuDisplayer menu = setMenu();
        menu.setInput();
        int answer = menu.getInput();
        System.out.println("\n");

        // BACK
        if (answer == 3) {
            System.out.println(CYAN + "Goodbye, I hope we will trade together again\n" + RESET);
            return;
        }
        // SELL
        else if (answer == 2) {
            this.done = true;
            this.trader.doAction(2);
        }
        // BUY
        else if (answer == 1) {
            this.done = true;
            this.trader.doAction(3);
        }
        // Error
        else {
            printError();
        }
        this.launch(c);
        return;
    }

    /**
     * Initialize attributes
     */
    public void createKeys(GameCharacter c) {
        // Cast of the trader and recuperation of the Hero
        this.trader = (Trader) c;
        this.player = trader.getCell().getHero();
    }

    /**
     * Create the menu when you talk to an Trader, you can only choose
     * between selling or buying
     * 
     * @return the menu which the launch() function will use
     */
    public MenuDisplayer setMenu() {
        // Create the header sentence
        String request = CYAN + "Hello ! I am " + this.trader.getName() + " , do you want to deal with me ?";
        request += CYAN + "\nYou can buy him some items" + RESET;
        request += GREEN + "\nOr you can sell him yours" + RESET;

        // Create selling option
        String selling[] = new String[1];
        selling[0] = "Sell an item to " + this.trader.getName();

        // Create buying option
        String buying[] = new String[1];
        buying[0] = "Buy an item to " + this.trader.getName();

        // Create back option:
        String back[] = new String[1];
        back[0] = "Goodbye";

        String choices[][] = new String[3][];
        choices[0] = buying;
        choices[1] = selling;
        choices[2] = back;

        String colors[] = new String[3];
        colors[0] = CYAN;
        colors[1] = GREEN;
        colors[2] = RESET;

        String separators[] = new String[3];
        separators[0] = "\n";
        separators[1] = "\n";
        separators[2] = "";

        MenuDisplayer menu = new MenuDisplayer(request, choices, separators, colors);
        return menu;
    }
}