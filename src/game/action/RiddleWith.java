package game.action;

import game.cell.Cell;
import game.character.GameCharacter;
import game.character.Hero;
import game.character.NPC.Sphynx;
import game.maze.Maze;
import game.menu.MenuDisplayer;
import game.menu.YesNoMenu;

import java.util.Random;

public class RiddleWith extends Action {

    private Sphynx sphynx;
    private String question;
    private String[] answers = new String[4];
    private String goodAnswer;
    private int goodIndex;
    private Random rand;
    private Hero player;

    public RiddleWith(Maze maze) {
        super(maze);
        this.name = "TALK TO A SPHYNX";
        this.rand = new Random();
    }

    public void launch(GameCharacter c) {
        //Print header
        System.out.println(BG_CYAN+LINE+this.name);

        createKeys(c);
        MenuDisplayer menu = setMenu();
        menu.setInput();
        int answer = menu.getInput();
        System.out.println("\n");

        // Answer to the enigma
        if (answer > 0 && answer < 5) {
            YesNoMenu confirmation = new YesNoMenu("Do you really want to answer " + this.answers[answer-1] + " ?");
            confirmation.setInput();
            System.out.println("\n");
            
            // Confirm answer
            if (confirmation.getInput()) {
                Cell currentCell = this.sphynx.getCell();
                if (answer == this.goodIndex) {
                    // Good answer
                    System.out.println(CYAN+ "Congratulation ! You gave me the good answer, so there is a clue to get out of this place"+ RESET);
                    System.out.println(this.sphynx.getName() +" dropped something for you on the floor !\n");
                    currentCell.addItem(this.sphynx.getReward());
                    this.sphynx.setCell(null);
                    System.out.println(this.sphynx.getName()+" starts to collapse in front of you. After some second, all that remains of it is dust");
                    currentCell.removeNPC(this.sphynx);
                    return;
                } else {
                    // bad answer
                    System.out.println(CYAN + "Unfortunatly, your answer is wrong... I cannot give you anything." + RESET);
                    this.sphynx.setCell(null);
                    System.out.println(this.sphynx.getName()+" starts to collapse in front of you. After some second, all that remains of it is dust");
                    currentCell.removeNPC(this.sphynx);
                    return;
                }
            }
            // Cancel answer
            else {
                System.out.println(CYAN + "Take all the time you will need and choose your answer wisely" + RESET);
                return;
            }
        }
        // Back
        else if (answer == 5) {
            System.out.println(CYAN + "I hope that I will see you again" + RESET);
            return;
        } else {
            printError();
        }
        this.launch(c);
        return;
    }

    public void createKeys(GameCharacter c) {
        // Cast of characters
        this.sphynx = (Sphynx) c;

        this.question = this.sphynx.getEnigma()[0];
        for (int i = 0; i < 4; i++) {
            answers[i] = this.sphynx.getEnigma()[i + 1];
        }
        this.goodAnswer = this.sphynx.getEnigma()[1];

        this.player = (Hero) this.sphynx.getCell().getHero();

        // Shuffle the list
        String temp;
        int random;
        for (int i = 0; i < 4; i++) {
            random = rand.nextInt(4);
            temp = this.answers[i];
            this.answers[i] = this.answers[random];
            this.answers[random] = temp;
        }

        // Find the good answer
        for (int i = 0; i < 4; i++) {
            if (this.answers[i].equals(this.goodAnswer)) {
                this.goodIndex = i + 1;
                break;
            }
        }
    }

    public MenuDisplayer setMenu() {
        String request =CYAN+ "Hello, " + this.player.getName() + ", I am " + this.sphynx.getName()
                + ". I hope that you have answers, because I have a question for you !";
        request += "\nBut be awared : after your answer, whichever is correct or not, I will disappear forever..."+RESET+"\n";
        request += "\n"+ CYAN + this.question + RESET;

        // Create answer options
        String answerNames[] = new String[4];
        for (int i = 0; i < 4; i++) {
            String answer = this.answers[i];
            answerNames[i] = answer;
        }

        // Create back options
        String back[] = new String[1];
        back[0] = "Cancel";

        String choices[][] = new String[2][];
        choices[0] = answerNames;
        choices[1] = back;

        String colors[] = new String[2];
        colors[0] = CYAN;
        colors[1] = RESET;

        String separators[] = new String[2];
        separators[0] = "   |   ";
        separators[1] = "";

        MenuDisplayer menu = new MenuDisplayer(request, choices, separators, colors);
        return menu;
    }

}