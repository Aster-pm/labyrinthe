package game.action;

import javax.sound.sampled.Line;

import game.character.GameCharacter;
import game.character.Hero;
import game.item.Item;
import game.maze.Maze;
import game.menu.MenuDisplayer;
import game.menu.YesNoMenu;

public class LookInventory extends Action {

    private int nbItems;
    private Item[] inventory;
    private Hero player;

    public LookInventory(Maze maze) {
        super(maze);
        this.name = "LOOK INTO INVENTORY";
    }

    /**
     * Display the inventory of the player, if he can Use or drop out an object
     */
    public void launch(GameCharacter c) {
        // Print header
        System.out.println(BG_GREEN + LINE + this.name);
        createKeys(c);
        // Initialization of the menu
        MenuDisplayer menu = setMenu();
        menu.setInput();
        int answer = menu.getInput();
        System.out.println("\n");

        // Back
        if (answer == nbItems + 1) {
            return;
        }
        // Inspect an object
        else if (answer > 0 && answer < nbItems + 1) {
            Item theItem = this.inventory[answer - 1];
            System.out.println("\t" + BG_GREEN + theItem.Inspect(false, 0) + RESET);
            YesNoMenu drop = new YesNoMenu(GREEN + "Do you want to drop this item ?" + RESET);
            drop.setInput();
            System.out.println("\n");

            if (drop.getInput()) {
                theItem.setPickedUp(false);
                this.player.removeItem(theItem);
                this.player.getCell().addItem(theItem);
                System.out.println("\t" + BG_GREEN + "You've put " + theItem.getName() + " down" + RESET);
            }
            this.launch(c);
        } else {
            printError();
            this.launch(c);
        }
        return;
    }

    /**
     * Initialise attributes which will be usefull to create the menu or do the
     * actions
     */
    public void createKeys(GameCharacter c) {
        Hero player = (Hero) c;
        this.player = player;
        this.inventory = this.player.getInventory();
        this.nbItems = this.count(inventory);
    }

    /**
     * Create a menu with String tabs of the objects
     */
    public MenuDisplayer setMenu() {
        String request = "";
        if (this.nbItems == 0) {
            request += "Your bag is empty";
        } else {
            request += "You open your backpack";
        }
        request += "\nYou have " + YELLOW + this.player.getGold() + " G" + RESET;

        String itemNames[] = new String[this.nbItems];
        // Create item options

        for (int i = 0; i < this.nbItems; i++) {
            Item theItem = this.inventory[i];
            itemNames[i] = theItem.getName();
        }

        // Create back option:
        String back[] = new String[1];
        back[0] = "Close your bag";

        String choices[][] = new String[2][];
        choices[0] = itemNames;
        choices[1] = back;

        String colors[] = new String[2];
        colors[0] = GREEN;
        colors[1] = RESET;

        String separators[] = new String[2];
        separators[0] = "\n\t";
        separators[1] = "";

        MenuDisplayer menu = new MenuDisplayer(request, choices, separators, colors);
        return menu;
    }
}
