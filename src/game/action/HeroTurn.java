package game.action;

import javax.sound.midi.SysexMessage;

import game.cell.Cell;
import game.character.*;
import game.character.NPC.NPC;
import game.item.Item;
import game.item.Valuable;
import game.maze.Maze;
import game.menu.MenuDisplayer;
import game.menu.YesNoMenu;

public class HeroTurn extends Action {

    private Hero player;
    private NPC[] cellNpc;
    private int nbNpc;
    private Item[] cellItems;
    private int nbItems;

    /**
     * Create the action which with the player can talk to NPCs, gather Items and
     * look into his inventory
     * He can also chose to go in another room, but he cannot go through the walls
     * (he has to whatch the map)
     * 
     * @param maze
     */
    public HeroTurn(Maze maze, String name) {
        super(maze);
        this.done = false;
        this.name = "CHOOSE YOUR ACTION";
    }

    /**
     * Move the player from the current Cell to a new Cell
     * 
     * @param newCell the destination cell
     */
    public void moveHero(Cell newCell) {
        Cell currentCell = this.maze.getCell(this.player.getCell().getX(), this.player.getCell().getY());
        currentCell.removeHero();
        this.player.setCell(newCell);
        newCell.SetHero(this.player);
    }

    /**
     * Make some modification in the maze : move the player or launch others actions
     * with another NPC
     * or put an Item into the Hero's inventory or quit the game
     */
    public void launch(GameCharacter c) {
        // Initialisation of attributes
        createKeys(c);

        // Print header of the action
        System.out.println(BG_WHITE + LINE + this.name);

        // Show the map
        this.maze.displayMaze();

        // Recuperation and discovering of the cell
        Cell currentCell = this.player.getCell();
        currentCell.setVisited(true);

        // Initialisation of the menu
        MenuDisplayer menu = setMenu();
        menu.setInput();
        int answer = menu.getInput();
        System.out.println("\n");

        // Modify the game depending of the answer
        if (answer > 0 && answer < 7 + this.nbNpc + this.nbItems) {

            // EXIT GAME
            if (answer == this.nbItems + this.nbNpc + 6) {
                YesNoMenu confirmation = new YesNoMenu("Are you sure that you want to quit ?");
                confirmation.setInput();
                if (confirmation.getInput()) {
                    System.out.println(RED+"\tG A M E    O V E R\n\n\n\n\n\n\n");
                    System.out.println(BG_RED + "You decided to give up, and sit on the floor");
                    System.out.println("You really hope that you will be safe for the rest of your life.");
                    System.out.println("̷Y̷o̴u̵ ̵s̵h̵o̶u̴l̵d̴n̸'̷t̶ ̷l̶i̷s̴t̶e̴n̴ ̷v̶o̸i̷c̵e̵s̷ ̸f̴r̵o̷m̷ ̸t̴h̴e̴ ̸u̷n̵d̴e̷r̶g̶r̸o̷u̸n̵d̶ ̷s̸i̴n̴c̴e̵ ̶n̵o̸w̸");
                    System.out.println("̴P̶l̶e̴a̵s̴e̵ ̶d̵o̸ ̸n̶o̴t̴ ̷r̴e̶s̵p̸o̸n̸d̴ ̴t̵o̵ ̵t̴h̸e̷m̴.̶");
                    System.out.println("̴Y̷o̷u̷ ̸m̶a̵y̷ ̷n̵o̶t̷ ̵b̴e̵ ̶s̷a̵n̵e̵ ̸a̶n̷y̵m̷o̷r̵e̶ ̸i̸f̸ ̶y̶o̵u̵ ̸d̸o̷");
                    System.out.println("̸͕͝R̸̪̒ȇ̵͓m̵͎̆e̷̙͋m̵̠̄b̵̺͒e̷̬͛r̶͎̍,̸͔̊ ̸̺͋d̸͉̂ō̴͈ ̶̛̻n̵̖̊o̵̙͒t̷̲͋ ̶̖̈́r̷̙̎ȇ̴̳ś̷̜p̴̭͒o̴̱͠ń̴͉d̷͎̏ ̶̞͛t̵͚́o̶̼̍ ̴̻͌ủ̴̮n̵͙͘d̴͇͘e̶̫͋ŗ̴̆g̵͎̕r̵̦̈́ȯ̷̜ũ̵̱n̶̤̏d̸̲̈́ ̷̨͝p̸̥̈ė̶̝o̸̲̕p̵̙̈́ḷ̷̐ḛ̴̒");
                    System.out.println("̷͖̾D̸͍͆o̵̪̓ ̸̬̈́ņ̵̎o̵͍͠t̸͇̔ ̷́ͅl̴͆ͅi̴̱͌s̶̪̚t̴̲͒ḙ̶͌n̷̢͗ ̶͖̒t̷̙̔h̵͚̏è̸̫ ̷͙̈́v̴̘̔o̵̧̓i̶͔͛c̴̺̓e̸̻̿ŝ̸̺,̸̡̓");
                    System.out.println("̶̮͚̈́Á̷̲̈́n̸̜̊͆d̴̍͜ ̷̻͂̉ṟ̷̨̉̔è̴̝͈m̸̞̔̓ȅ̴̖̝m̵͎̐b̶̬̓̓e̵̠͒r̵̢̖͛.̵̮̅̌.̶̮̫͊.̸͙̓̀");
                    System.out.println("̴̹̺̍̀̊ỳ̶̹ỏ̵̧̹͑̚u̶̟̐͌r̸̡̟͗̅̓ ̸̛̬̕v̸̟̩̪͐̈́͠ó̷͖̉̓͜ĩ̷̧ͅc̶̖͐͑ȇ̷͕̩̖̕");
                    System.exit(0);
                } else {
                    this.launch(c);
                }
                return;
            }

            // Move to North if it's possible
            if (answer == 4) {
                if (currentCell.checkWall("n") || currentCell.getX() - 1 < 0) {
                    System.err.println("You try to go North, but there's a wall, try another direction.");
                } else {
                    this.moveHero(maze.getCell(currentCell.getX() - 1, currentCell.getY()));
                }
            }
            // Move to South if it's possible
            else if (answer == 2) {
                if (currentCell.checkWall("s") || currentCell.getX() + 1 >= this.maze.getWidth()) {
                    System.err.println("You try to go South, but there's a wall, try another direction.");
                } else {
                    this.moveHero(maze.getCell(currentCell.getX() + 1, currentCell.getY()));
                }
            }
            // Move to East if it's possible
            else if (answer == 3) {
                if (currentCell.checkWall("e") || currentCell.getY() + 1 >= this.maze.getHeight()) {
                    System.err.println("You try to go East, but there's a wall, try another direction.");
                } else {
                    this.moveHero(maze.getCell(currentCell.getX(), currentCell.getY() + 1));
                }
            }
            // Move to West if it's possible
            else if (answer == 1) {
                if (currentCell.checkWall("w") || currentCell.getY() - 1 < 0) {
                    System.err.println("You try to go West, but there's a wall, try another direction.");
                } else {
                    this.moveHero(maze.getCell(currentCell.getX(), currentCell.getY() - 1));
                }
            }
            // Look into inventory
            else if (answer == 5) {
                this.player.doAction(1);
            }
            // Talk to an NPC
            else if (answer > 5 && answer < this.nbNpc + 6) {
                NPC theNpc = this.cellNpc[answer - 6];
                System.out.println("\t" + BG_CYAN + "You talk with " + theNpc.getName() + RESET);
                theNpc.doAction(1);
            }
            // Take an item
            else if (answer > this.nbNpc + 5 && answer < this.nbItems + nbItems + 6) {
                // test if the inventory is not full or if the item is gold
                if (this.count(player.getInventory()) < player.getInventory().length
                        || currentCell.getOneItem(answer - 6 - nbNpc) instanceof Valuable) {
                    try {
                        Item theItem = currentCell.getOneItem(answer - 6 - nbNpc);
                        theItem.pickUp(answer - 6 - nbNpc);
                        if (theItem instanceof Valuable) {
                            player.addGold(theItem.getbuyValue());
                            System.out.println(
                                    "\t" + BG_BLUE + "You found " + YELLOW + theItem.getbuyValue() + "G" + BG_BLUE
                                            + " on the floor !" + RESET);
                        } else {
                            System.out.println(
                                    "\t" + BG_BLUE + "You found " + theItem.getName() + " on the floor !" + RESET);
                            player.addItem(theItem);
                        }
                    } catch (Exception e) {
                        System.out.println(RED + e.getMessage() + RESET);
                        this.launch(c);
                    }
                }
                // the inventory is full
                else {
                    System.out.println("\t" + BG_GREEN + "Your inventory is full" + RESET);
                    this.launch(c);
                }
            }
        } else {
            printError();
            this.launch(c);
        }
        return;
    }

    /**
     * Initialize attributes
     */
    public void createKeys(GameCharacter c) {
        // cast of the Hero
        this.player = (Hero) c;
        Cell currentCell = this.player.getCell();

        // Tab for the characters in the cell
        this.nbNpc = this.count(currentCell.getNPC());
        this.cellNpc = new NPC[this.nbNpc];

        for (int i = 0; i < this.nbNpc; i++) {
            this.cellNpc[i] = currentCell.getNPC()[i];
        }

        // Tab for the items in the cell
        this.nbItems = this.count(currentCell.getItem());
        this.cellItems = new Item[nbItems];

        for (int i = 0; i < nbItems; i++) {
            this.cellItems[i] = currentCell.getItem()[i];
        }
    }

    /**
     * Create the main Menu during a turn of the player, it's the most complete menu
     * with a lot of options.
     * It create the 4 directions, show each character in the Cell, and each Item
     * that he can gather
     * 
     * @return the menu which the launch() function will use to modify the game
     */
    public MenuDisplayer setMenu() {
        // recuperation of the cell
        Cell currentCell = this.player.getCell();

        // Create the sentence :
        String sentence = GREEN + this.player.getName() + RESET + ", you are in the room [" + currentCell.getX() + " : "
                + currentCell.getY() + "] . What do you want to do ?";

        // Create directions list :
        String directions[] = new String[4];
        directions[3] = "[ʌ]  go to North";
        directions[1] = "[v]  go to South";
        directions[2] = "[>]  go to East";
        directions[0] = "[<]  go to West";

        // create inventory option :
        String inventory[] = new String[1];
        inventory[0] = "Look into your bag";

        // Create characters list:
        String characterNames[] = new String[this.nbNpc];
        for (int i = 0; i < this.nbNpc; i++) {
            characterNames[i] = "Talk to " + cellNpc[i].getName();
        }

        // Create items list:
        String itemNames[] = new String[this.nbItems];
        for (int i = 0; i < this.nbItems; i++) {
            itemNames[i] = "Take the " + cellItems[i].getClass().getSimpleName() + " on the floor";
        }

        // Create back option:
        String back[] = new String[1];
        back[0] = "QUIT GAME";

        // Create the choice list for the menu:
        String choices[][] = new String[5][];
        choices[0] = directions;
        choices[1] = inventory;
        choices[2] = characterNames;
        choices[3] = itemNames;
        choices[4] = back;

        // Create the separators list for the options:
        String separators[] = new String[5];
        separators[0] = "   .   ";
        separators[1] = "";
        separators[2] = "  /  ";
        separators[3] = "  \\  ";
        separators[4] = "";

        // Create the color list for the options:
        String colors[] = new String[5];
        colors[0] = WHITE;
        colors[1] = GREEN;
        colors[2] = CYAN;
        colors[3] = BLUE;
        colors[4] = RESET;

        // Finally, create the menu
        MenuDisplayer menu = new MenuDisplayer(sentence, choices, separators, colors);
        return menu;
    }
}