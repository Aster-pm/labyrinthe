package game.action;

import game.character.GameCharacter;
import game.character.NPC.Explorer;
import game.maze.Maze;
import game.menu.MenuDisplayer;
import game.menu.YesNoMenu;

public class ExplorerTalk extends Action {

    private Explorer explorer;
    private String clue;

    public ExplorerTalk(Maze maze) {
        super(maze);
        this.name = "TALK TO AN EXPLORER";
    }

    public void launch(GameCharacter c) {
        // Print header
        System.out.println(BG_CYAN + LINE + this.name);
        createKeys(c);
        MenuDisplayer menu = setMenu();
        menu.setInput();
        int answer = menu.getInput();
        System.out.println("\n");

        // Talk with the Explorer
        if (answer == 1) {
            System.out.println(CYAN + "Oh you're lost too ? Maybe I can help you to find your way..." + RESET+ "\n");
            // confirmation
            YesNoMenu confirmation = new YesNoMenu(
                    CYAN + "Do you want hint about what you have to do to get out ?" + RESET);
            confirmation.setInput();
            System.out.println("\n");

            // YES
            if (confirmation.getInput()) {
                System.out.println("\t" + BG_CYAN + "I'm not sure, but I think that you have to "
                        + this.explorer.getClue() + RESET);
                System.out.println(CYAN + "I hope that helped you, good luck !" + RESET);
                return;
            } else {
                System.out.println(
                        CYAN + "Okay, I can understand that you what to do it only by yourself, good luck !" + RESET);
                return;
            }
        }
        // BACK
        else if (answer == 2) {
            System.out.println(CYAN + "Okay bye, good luck !" + RESET);
            return;
        }
        // Error
        else {
            printError();
        }
        this.launch(c);
        return;
    }

    public void createKeys(GameCharacter c) {
        this.explorer = (Explorer) c;
        this.clue = this.explorer.getClue();
    }

    public MenuDisplayer setMenu() {
        String request = CYAN + "Hello there, I'm " + this.explorer.getName()
                + " I've walked through this maze for days\n" + RESET;

        String hello[] = new String[1];
        hello[0] = "Hello...";

        String back[] = new String[1];
        back[0] = "BACK";

        String choices[][] = new String[2][];
        choices[0] = hello;
        choices[1] = back;

        String separators[] = new String[2];
        separators[0] = "";
        separators[1] = "";

        String colors[] = new String[2];
        colors[0] = RESET;
        colors[1] = RESET;

        MenuDisplayer menu = new MenuDisplayer(request, choices, separators, colors);
        return menu;
    }

}