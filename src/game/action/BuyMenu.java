package game.action;

import game.character.GameCharacter;
import game.character.Hero;
import game.character.NPC.Trader;
import game.item.Item;
import game.maze.Maze;
import game.menu.MenuDisplayer;
import game.menu.YesNoMenu;

public class BuyMenu extends Action {
    private Item[] store;
    private int nbItems;
    private Trader trader;
    private Hero player;

    public BuyMenu(Maze maze) {
        super(maze);
        this.done = false;
        this.name = "BUY SOMETHING";
    }

    public void launch(GameCharacter c) {
        // Print header of action
        System.out.println(BG_CYAN + LINE + this.name);
        // Initialisation of the menu
        createKeys(c);
        MenuDisplayer menu = setMenu();
        menu.setInput();
        int answer = menu.getInput();
        System.out.println("\n");

        // Buy the object
        if (answer > 0 && answer < this.nbItems + 1) {
            Item theItem = this.store[answer - 1];

            // Player's inventory is full
            if (count(this.player.getInventory()) == this.player.getInventory().length) {
                System.out
                        .println(CYAN + "I cannot sell you anithing, you have no more place in your inventory" + RESET);
            }

            // Player has not enough money
            else if (theItem.getbuyValue() > this.player.getGold()) {
                System.out.println(CYAN + "Hey, you don't have enough money to buy " + theItem.getName() + RESET);
            } else {
                // Confirmation
                String conf = CYAN + "Are you sure that you want to buy " + theItem.getName() + " for " + YELLOW
                        + theItem.getbuyValue() + "G" + CYAN + " ?" + RESET;
                YesNoMenu confirmation = new YesNoMenu(conf);
                confirmation.setInput();
                System.out.println("\n");

                if (confirmation.getInput()) {
                    // Move the item and earn money
                    this.trader.removeItem(theItem);
                    this.trader.addGold(theItem.getsellValue());
                    this.player.addItem(theItem);
                    this.player.takeGold(theItem.getbuyValue());
                    theItem.setPickedUp(true);
                    System.out.println("\t" + BG_CYAN + "Thank you, here is your " + theItem.getName() + RESET);
                }
            }

        }
        // Back
        else if (answer == this.nbItems + 1) {
            return;
        } else {
            printError();
        }
        this.launch(c);
        return;
    }

    public void createKeys(GameCharacter c) {
        // Get the Character
        this.trader = (Trader) c;
        this.player = this.trader.getCell().getHero();
        // Initialize attributes
        this.store = this.trader.getInventory();
        this.nbItems = count(this.store);
    }

    public MenuDisplayer setMenu() {
        String request = CYAN + "What do you want to buy ?\n" + RESET;
        request += CYAN + this.trader.getName() + " has " + YELLOW + this.trader.getGold() + " gold\n" + RESET;
        request += GREEN + "You have " + YELLOW + this.player.getGold() + " gold\n" + RESET;

        // Empty inventory Trader
        if (count(this.store) == 0) {
            request += CYAN + "\nSorry I have no more items to sell..." + RESET;
        }

        // Create Item lists
        String inventoryName[] = new String[this.nbItems];
        Item theItem;
        for (int i = 0; i < this.nbItems; i++) {
            theItem = this.store[i];
            inventoryName[i] = theItem.getName() + " : " + theItem.getbuyValue();
        }

        // Create Back option
        String back[] = new String[1];
        back[0] = "Don't buy anymore";

        // Create choices list
        String choices[][] = new String[2][];
        choices[0] = inventoryName;
        choices[1] = back;

        // Create separators
        String separators[] = new String[2];
        separators[0] = "\n\t";
        separators[1] = "";

        // Create color list
        String colors[] = new String[2];
        colors[0] = CYAN;
        colors[1] = RESET;
        MenuDisplayer menu = new MenuDisplayer(request, choices, separators, colors);
        return menu;
    }

}
