package game.action;
import game.character.GameCharacter;
import game.maze.*;
import game.menu.*;

public abstract class Action {

	//Colors can be used in all classes of Action (for setMenu)
	//Put those before a String start to color all characters after in the terminal
	protected static final String BLACK = "\033[0;30m";
	protected static final String RED = "\033[0;31m";
	protected static final String GREEN = "\033[0;32m";
	protected static final String YELLOW = "\033[0;33m";
	protected static final String BLUE = "\033[0;34m";
	protected static final String PURPLE = "\033[0;35m";
	protected static final String CYAN = "\033[0;36m";
	protected static final String WHITE = "\033[0;37m";
	//Backgroud colors
	protected static final String BG_BLACK = "\033[0;40m";
	protected static final String BG_RED = "\033[0;41m";
	protected static final String BG_GREEN = "\033[0;42m";
	protected static final String BG_YELLOW = "\033[0;43m";
	protected static final String BG_BLUE = "\033[0;44m";
	protected static final String BG_PURPLE = "\033[0;45m";
	protected static final String BG_CYAN = "\033[0;46m";
	protected static final String BG_WHITE = "\033[0;47m";
	//Reset at the end of a string to stop coloring
	protected static final String RESET = "\u001B[0m";
	//Print a white line
	protected static final String LINE = "\n"+RESET+"\n";

    protected Maze maze;
	protected String name;
	protected boolean done = false;
	
	/** Create the action and initialize his name
	 * @param maze
	 */
    public Action(Maze maze) {
		this.maze = maze;
	}

	/**@return the maze where the action act
	 */
	public Maze getMaze(){
		return this.maze;
	}

	/**@return true to say if the action has been done
	 */
	public boolean getDone(){
		return this.done;
	}

	/**@return name the name of the Acion
	 */
	public String getName(){
		return this.name;
	}

	/** Print an error message, used case where the answer given doesn't exist in a menu)
	 */
	public void printError(){
		System.err.println("\t"+BG_RED+"! your answer doesn't match with anything, type another number !"+RESET);
	}

	public int count(Object[] tab){
		int number = 0;
		for(int i=0;i<tab.length;i++){
			if(tab[number]!=null){
				number +=1;
			}
		}
		return number;
	}

	/**
	 * Execute an action on/by Hero h
	 * @param c GameCharacter
	 */
	public abstract void launch(GameCharacter c);

	public abstract void createKeys(GameCharacter c);

	/**
	 * Return a String who can be Diplayed by the Menu Displayer
	 * @param c
	 * @return MenuDisplayer
	 */
	public abstract MenuDisplayer setMenu();
}