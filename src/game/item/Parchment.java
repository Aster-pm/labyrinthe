package game.item;

public class Parchment extends Item {

    private String clue;

    /**
     * Initialize a Valuable item, an item who's only purpose is to give player's
     * money and stores it immediatly upon picking it up.
     * 
     * @param name  the name of the item
     * @param value item's gold worth, this is the value the player will get by
     *              picking it up
     * @param maxV
     * @param minV
     */
    public Parchment(String name, int sellValue, String clue) {
        super(name, sellValue);
        this.sellValue = 0;
        this.clue = clue;
    }

    /**
     * return the clue write on the parchment
     * 
     * @return String
     */
    public String getClue() {
        return this.clue;
    }

    /**
     * set the clue write on the parchment
     * 
     * @param clue the new clue
     */
    public void setClue(String clue) {
        this.clue = clue;
    }

    /**
     * inspect the parchment
     * 
     * @return String
     */
    public String Inspect(boolean trade, int... i) {
        String toShow;
        if (trade) {
            return "Le marchand vous vend " + this.getName() + " pour " + this.getsellValue() + "g";
        }
        if (!this.pickedUp) {
            // toShow = "Recuperez le parchemin pour pouvoir lire ce qu'il y a d'écrit
            // dessus";

            toShow = "You open " + this.getName() + " and read : The legend says that you have to " + this.clue;

        } else {
            toShow = "You open " + this.getName() + " and read : The legend says that you have to " + this.clue;

        }
        return toShow;
    }
}
