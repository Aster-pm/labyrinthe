package game.item;
import game.cell.*;

public abstract class Item {
    protected String name;
    protected Cell currentCell;
    protected boolean pickedUp;
    protected int sellValue;
    protected int buyValue;


    /** Initialize a new Maze
	 * @param name The name of the item
	 */
    public Item(String name,int sellValue){
        this.pickedUp = false;
        this.name = name;
        this.currentCell = null;
        this.sellValue = sellValue;
        this.buyValue = sellValue*2;
    }

    /**
     * return the name of this item
     * @return String
     */
    public String toString(){
        return this.name;
    }

    /**
     * return the value for sell
     * @return int
     */
    public int getsellValue(){
        return this.sellValue;
    }

    /**
     * return the value for buy
     * @return int
     */
    public int getbuyValue(){
        return this.buyValue;
    }

    /**
     * Set item's cell/position
     * @param c the Cell where the Item is
     */
    public void setCell(Cell c){
        this.currentCell = c;
    }


    /**
     * return the name of the Item
     * @return String
     */
    public String getName(){
        return this.name;
    }


    /**
     * return the cell where the Item is (null if item isnt in a cell)
     * @return Cell
     */
    public Cell getCell() {
        return this.currentCell;
    }

    /**
     * return the boolean status of the Item's pickedUp attribute
     * @return boolean
     */
    public boolean getPickedUp() {
        return this.pickedUp;
    }

    /**
     * set the boolean status of the Item's pickedUp attribute
     */
    public void setPickedUp(boolean state) {
        this.pickedUp = state;
    }


    /**
     * gives you a small description of the item, return value vary depending on its position
     * @return String
     */
    public abstract String Inspect(boolean trade,int ... i);

    /**
     * gives you a small description of the item, return value vary depending on its position
     * @return String
     */
    // public String InspectSell(int ... i){
    //     if(!this.pickedUp){
    //         return "Le marchand vous vend " + this.getName() + " pour " + this.getbuyValue() + "g";
    //     }
    //     else{
    //         return Inspect(i);
    //     }
    // }


    /**
     * Trigger the action to pick up the item if it's in a cell and add it's gold value to the player's total gold (player not implemented yet)
     * @return Item
     * @throws AlreadyPickedUpException if the item cannot be picked up (ie it's not in a Cell)
     */
    public Item pickUp(int i) throws AlreadyPickedUpException{
        if(!this.pickedUp){
            this.pickedUp = true;
            this.currentCell.removeItem(i);
            this.currentCell = null;

            return this;
        }
        else{
            throw new AlreadyPickedUpException("cet Item à déjà été ramassé");
        }
    }
}
