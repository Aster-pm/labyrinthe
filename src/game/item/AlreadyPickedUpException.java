package game.item;

public class AlreadyPickedUpException extends Exception {
    
    public AlreadyPickedUpException() {
        super();
    }

    public AlreadyPickedUpException(String message) {
        super(message);
    }
}
