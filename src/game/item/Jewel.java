package game.item;


public class Jewel extends Item{

  private JewelEnum type;
  /*le type de joyaux n'a pas encore d'utilité car la classe Jewel est encore très similaire a Valuables. 
  Dans le futur les jewel seront différents et devront être vendus pour obtenir leur valeur (ce qui explique les différents type)*/

  public Jewel(String name, int sellValue, JewelEnum j){
    super(name,sellValue);
    this.type = j;
  }

    public String Inspect(boolean trade,int ... i){
        if(trade){
          return "Vous pouvez vendre " + this.getName() + " pour " + this.getsellValue() + "g";
        }
        if(!this.pickedUp){
            return "Le marchand vous vend " + this.getName() + " pour " + this.getbuyValue() + "g";
        }
        else{
          return this.getName()+" is a "+this.getClass().getSimpleName()+", its value is "+this.getsellValue();
        }
    }
  
  /**
     * gives you a small description of the item, since it cannot be stored in an inventory there's no position dependance 
     * @return the status of the item and it's gold worth
     */
    // public String InspectSell(int ... i){
    //     if(!this.pickedUp){
    //         return "Le marchand vous vend " + this.getName() + " pour " + this.getbuyValue() + "g";
    //     }
    //     else{
    //         return Inspect(i);
    //     }
    // }

}
