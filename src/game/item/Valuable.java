package game.item;

public class Valuable extends Item{


    /**
     * Initialize a Valuable item, an item who's only purpose is to give player's money and stores it immediatly upon picking it up.
     * @param name the name of the item
     * @param value item's gold worth, this is the value the player will get by picking it up
     */
    public Valuable(String name,int value){
        super(name,value);
    }

    public String Inspect(boolean trade,int ... i){
        if(trade){
            return "Le marchand vous vend " + this.getName() + " pour " + this.getsellValue() + "g";
        }
        if(!this.pickedUp){
            return "Le marchand vous vend " + this.getName() + " pour " + this.getbuyValue() + "g";
        }
        else{
            return this.getName()+" is a "+this.getClass().getSimpleName()+", its value is "+this.getsellValue();
        }
    }

    // /**
    //  * gives you a small description of the item, since it cannot be stored in an inventory there's no position dependance 
    //  * @return the status of the item and it's gold worth
    //  */
    // public String Inspect(int ... i){
    //     String toShow = "Il y à un-e" + getName() + "dans cette salle qui vaut" + getsellValue();
    //     return toShow;
    // }

}
