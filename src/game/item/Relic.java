package game.item;

import java.util.Random;

public class Relic extends Item{
    private int maxV;
    private int lastTurn;


    /**
     * Initialize a Valuable item, an item who's only purpose is to give player's money and stores it immediatly upon picking it up.
     * @param name the name of the item
     * @param value item's gold worth, this is the value the player will get by picking it up
     * @param maxV 
     * @param minV
     */
    public Relic(String name,int value){
        super(name,value);
        this.maxV = value *10;
    }


    /**
     * Change the gold value of the NFT each time it's called, it can go over or under the base price. Not a 1 to 1 representation because NFT usually tends to lose their worth instead of gaining some
     */
    public void fluctuate(int turn){
        if(turn > lastTurn){
            lastTurn++;
            this.sellValue = (int) Math.round(Math.random()*this.maxV);
            this.maxV = (int) this.maxV/2;
        }
    }


    public String Inspect(boolean trade,int ... i){
        if(trade){
            return "Le marchand vous vend " + this.getName() + " pour " + this.getsellValue() + "g";
        }
        if(!this.pickedUp){
            return "Le marchand vous vend " + this.getName() + " pour " + this.getbuyValue() + "g";
        }
        else{
            return this.getName()+" is a "+this.getClass().getSimpleName()+", its value is "+this.getsellValue();
        }
    }
  

    // /**
    //  * gives you a small description of the item, since it cannot be stored in an inventory there's no position dependance 
    //  * @return the status of the item and it's gold worth
    //  */
    // public String Inspect(int state){

    //     String toShow = "Il y à un NFT de " +this.name +" dans cette salle qui vaut " + getsellValue()+"G est-ce que vous aimez prendre des risques et potentiellement perdre toute votre argent en misant sur un système pyramidale ?";
    //     return toShow;
    // }

        /**
     * gives you a small description of the item, since it cannot be stored in an inventory there's no position dependance 
     * @return the status of the item and it's gold worth
     */
    public String Inspect(int state, int turn){
        this.fluctuate(turn);
        String toShow = "Il y à un NFT de " +this.name +" dans cette salle qui vaut " + getsellValue()+"G est-ce que vous aimez prendre des risques et potentiellement perdre toute votre argent en misant sur un système pyramidale ?";
        return toShow;
    }

}
    
    
