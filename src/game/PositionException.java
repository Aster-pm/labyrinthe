package game;

public class PositionException extends Exception {
	public PositionException(String s) {
		super(s);
	}
}
