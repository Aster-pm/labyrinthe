package game;

import java.io.ObjectStreamException;
//je sais pas si on a besoin de tout ca faudrait voir pour réduire la liste
import java.util.*;

import javax.crypto.ExemptionMechanism;

import game.GameDataReader;
import game.Objective;
import game.cell.*;
import game.character.*;
import game.maze.Maze;
import game.mazeAlgo.*;
import game.character.NPC.*;
import game.item.*;
import game.action.*;

public class jeu {
    // DEFINITION DES COULEURS
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    private Maze theMaze;
    private Hero player;
    private NPC[] nonplayers;
    private boolean end;
    private Item[] tradeItems;
    private int nbOfTradeitems;
    private Item[] items;
    public Objective objective;
    private int totalGold;

    public jeu(Maze theMaze, Hero player) {
        // création du labyrinthe
        this.theMaze = theMaze;
        // initialisation du joueur et des NPC
        this.nonplayers = new NPC[300];
        this.player = player;
        this.items = new Item[300];
        this.end = false;
        this.tradeItems = new Item[300];
        this.nbOfTradeitems = 0;
        this.totalGold = 0;

        this.createObjective();
    }

    public Maze getMaze() {
        return this.theMaze;
    }

    /**
     * @param i index of the item
     * @return item of index i
     */
    private Item getGroundItem(int i) {
        return this.items[i];
    }

    private Item[] getGroundItem() {
        return this.items;
    }

    private Item[] getTradeItems() {
        return this.tradeItems;
    }

    /**
     * Return random nb Item from this.tradeItems, it will also sort the array so
     * you have every item at the start and null at the end once you took an item
     * from the list.
     * 
     * @param nb the number of item you want to return
     * @return a list of item randomly picked from this.tradeItemsS
     */
    private Item[] getSomeTradeItems(int nb) {
        Item[] toReturn = new Item[nb];
        Item currItem;
        int index;

        for (int i = 0; i < nb && this.nbOfTradeitems > 0; i++) {
            index = 0;
            currItem = null;
            while (currItem == null) {
                index = (int) (Math.random() * this.nbOfTradeitems);
                currItem = this.tradeItems[index];

            }
            this.tradeItems[index] = null;
            this.nbOfTradeitems -= 1;
            toReturn[i] = currItem;
        }

        // remet les items au debut et null à la fin
        int j, y;
        j = -1;
        y = 0;
        while (y < this.tradeItems.length) {
            // System.out.println("Game.getSomeTradeItems()");

            if (this.tradeItems[y] != null) {
                j++;
                Item temp = this.tradeItems[y];
                this.tradeItems[j] = this.tradeItems[y];
                this.tradeItems[y] = temp;
            }
            y++;
        }

        return toReturn;
    }

    public void randomizeNPC() {
        Random rand = new Random();

        for (int i = 0; i < this.nonplayers.length; i++) {
            if (this.nonplayers[i] != null) {
                NPC npc = this.nonplayers[i];
                Cell currentCell = npc.getCell();
                int newX = rand.nextInt(this.theMaze.getWidth());
                int newY = rand.nextInt(this.theMaze.getHeight());
                Cell newCell = this.theMaze.getCell(newX, newY);
                npc.setCell(newCell);
                currentCell.removeNPC(npc);
                newCell.addNPC(npc);
            }
        }
    }

    /**
     * Randomize the game items so most items can be found in dead ends. It will
     * distribute first every parchments, then jewels, then nfts, then goldpiles.
     * If there are no more dead ends to fill, it will randomly pick a cell to
     * deposit the item.
     */
    public void randomizeItems() {

        // On récupère toutes les dead ends
        ArrayList<Cell> cells = new ArrayList<>();
        int i = 0;
        for (Cell[] row : this.theMaze.getCells()) {
            for (Cell c : row) {
                if (c.nbOfWall() == 3) {
                    cells.add(c);
                }
            }
        }
        Random rand = new Random();

        while (i < this.items.length & !cells.isEmpty()) {

            if (this.items[i] != null) {
                int index = rand.nextInt(cells.size());

                this.items[i].setCell(cells.get(index));
                cells.get(index).addItem(this.items[i]);

                // this.items[i].setCell(this.theMaze.getCell(0, 0));
                // this.theMaze.getCell(0, 0).addItem(this.items[i]);
                cells.remove(index);
            }

            i++;
        }

        while (i < this.items.length) {
            if (this.items[i] != null) {
                int newX = rand.nextInt(this.theMaze.getHeight());
                int newY = rand.nextInt(this.theMaze.getHeight());
                Cell newCell = this.theMaze.getCell(newX, newY);
                newCell.addItem(this.items[i]);
                this.items[i].setCell(newCell);
            }
            i++;
        }
    }

    public void startPlaying() {
        while (!this.end) {
            this.playTurn();
            if (this.objective.checkObjectives(this.player)) {
                end = true;
            }
        }
        String endHeader = "You managed to complete all the objectives necessary and you found the exit";
        System.out.println(ANSI_RED + endHeader + ANSI_RESET);
    }

    public void playTurn() {
        player.addTurn(1);
        this.player.doAction(0);
        System.out.println("\n");
        if (this.player.getTurn() % 5 == 0) {
            System.out.println("You heard footstep, somebody walked near your room in this maze...");
            for (NPC npc : nonplayers) {
                if (npc != null && !(npc instanceof Sphynx)) {
                    npc.doAction(0);
                }
            }
        }
        int min = this.player.getTurn();
        System.out.println("You have spent " + (int) min / 60 + " H. " + min % 60 + " MIN. here");
    }

    public void createObjective() {
        Random rng = new Random();
        Action[] possibleActions = { /* new TradeWith(theMaze, "il faut trade") */ };
        Item[] items = new Item[8];

        if (rng.nextInt(2) == 1) {
            int nbJewel = rng.nextInt(theMaze.getHeight() / 2);

            // System.out.println("nb jewel : "+nbJewel);

            int nbRelics = rng.nextInt(theMaze.getHeight() / 2);

            // System.out.println("nb nft : "+nbRelics);

            for (int i = 0; i < nbJewel; i++) {
                items[i] = new Jewel("objective jw " + i, 0, JewelEnum.ruby);
            }
            for (int i = 0; i < nbRelics; i++) {
                items[i + nbJewel] = new Relic("objective relic " + i, 0);
            }
        }

        int gold;
        if (rng.nextInt(2) == 1) {
            gold = rng.nextInt(5000) + 1000;
        } else {
            gold = 0;
        }
        this.objective = new Objective(
                this.theMaze.getCell(8, 3), 5,
                possibleActions, items, this.theMaze);
    }

    /**
     * Create a given numbers of each items to put randomly inside the maze and
     * adjust the total gold you can get by selling them so you should be able to
     * sell items to get at least enough
     * for twice of the objective required gold value
     * 
     * @param nbJewels  The number of Jewels you want to create
     * @param nbScrolls The number of Scrolls you want to create
     * @param nbRelics  The number of Nfts you want to create
     */
    public void createGroundItems() {
        int i = 0;
        int scrolls, nfts, jewels;

        int nbScrolls = 50;
        int nbJewel = 50;
        int nbRelics = 50;

        GameDataReader Names = new GameDataReader("#");
        HashMap<String, String[]> itemData = new HashMap<>();

        // récupération des noms d'objets
        Names.setData("item.txt");
        for (String str : Names.getData()) {
            // System.out.println("str : " + str.split("::")[1].split("\n")[0]);
            // System.out.println("jsp.split :" + jsp.split(":"));
            itemData.put(str.split("::")[0], str.split("::")[1].split("\n"));
        }

        for (scrolls = 0; scrolls < nbScrolls; scrolls++, i++) {
            String[] currentScrolls = itemData.get("parchment");

            this.items[i] = new Parchment(currentScrolls[scrolls % currentScrolls.length],
                    500, this.objective.giveRandomClue());

        }

        for (jewels = 0; jewels < nbJewel; jewels++, i++) {
            JewelEnum j;

            Random rngJewel = new Random();
            JewelEnum je = null;

            switch (rngJewel.nextInt(5)) {
                case 0:
                    je = JewelEnum.saphire;
                    break;
                case 1:
                    je = JewelEnum.ruby;
                    break;
                case 2:
                    je = JewelEnum.diamond;
                    break;
                case 3:
                    je = JewelEnum.topaze;
                    break;
                case 4:
                    je = JewelEnum.emerald;
                    break;
            }

            String[] currentjewel = itemData.get(je.toString());
            this.items[i] = new Jewel(currentjewel[nbJewel % currentjewel.length].split(":")[0],
                    Integer.parseInt(currentjewel[nbJewel % currentjewel.length].split(":")[1].trim()), je);

            this.totalGold += Integer.parseInt(currentjewel[nbJewel % currentjewel.length].split(":")[1].trim());

        }

        for (nfts = 0; nfts < nbRelics; nfts++, i++) {
            String[] currentRelic = itemData.get("relic");
            this.items[i] = new Relic(currentRelic[nfts % currentRelic.length].split(":")[0],
                    Integer.parseInt(currentRelic[nfts % currentRelic.length].split(":")[1].trim()));
            this.totalGold += Integer.parseInt(currentRelic[nfts % currentRelic.length].split(":")[1].trim());

        }

        // Creations de pile d'or pour équilibrer l'or qu'il faut pour compléter le
        // labyrinthe jusqua ce qu'il y ait au moins 2 fois plus d'or que nécessaire.

        int goldDiff;
        int toAdd;

        int nbItems = scrolls + nfts + jewels;
        while (this.objective.getGold() > (this.totalGold * 2) & nbItems < 63) {
            goldDiff = this.objective.getGold() - this.totalGold;

            if (goldDiff > 250 || goldDiff < 0) {
                toAdd = (int) (Math.random() * 250) + 100;

            } else {
                toAdd = (int) (Math.random() * goldDiff) + 100;

            }
            this.items[i] = new Valuable("Pile d'or de " + toAdd + "g", toAdd);
            this.totalGold += toAdd;
            i++;
            nbItems++;
        }
        if (this.objective.getGold() > (this.totalGold * 2)) {
            goldDiff = Math.abs(this.objective.getGold() - (this.totalGold) * 2);
            this.items[i] = new Valuable("Pile d'or de " + goldDiff + "g", goldDiff);
            this.totalGold += goldDiff;
        }
    }

    /**
     * Create a given numbers of each items to give to the trader's inventory so
     * they can sell it to the player.
     * Not implemented yet, but it should adjust the economy so you can generate
     * enough gold inside the maze to both buy most of traders stock and still be
     * able to complete objectives gold
     * 
     * @param nbJewels  The number of Jewels you want to create
     * @param nbScrolls The number of Scrolls you want to create
     * @param nbRelics  The number of Nfts you want to create
     */
    public void createTradeItems() {
        int i = 0;
        GameDataReader Names = new GameDataReader("#");
        HashMap<String, String[]> itemData = new HashMap<>();

        // récupération des noms d'objets
        Names.setData("item.txt");
        for (String str : Names.getData()) {
            // System.out.println("str : " + str.split("::")[1].split("\n")[0]);
            String jsp = str.split("::")[1];
            // System.out.println("jsp.split :" + jsp.split(":"));
            itemData.put(str.split("::")[0], str.split("::")[1].split("\n"));
        }

        int nbScrolls = (int) 50;
        int nbJewel = (int) 50;
        int nbRelics = (int) 50;

        Names.setData("parchment.txt");
        for (int scrolls = 0; scrolls < nbScrolls; scrolls++, i++) {

            String[] currentScrolls = itemData.get("parchment");

            this.tradeItems[i] = new Parchment(
                    currentScrolls[i % currentScrolls.length] + i,
                    500, this.objective.giveRandomClue());

            this.totalGold -= 500;

        }

        for (int jewels = 0; jewels < nbJewel; jewels++, i++) {

            Random rngJewel = new Random();
            JewelEnum je = null;

            switch (rngJewel.nextInt(5)) {
                case 0:
                    je = JewelEnum.saphire;
                    break;
                case 1:
                    je = JewelEnum.ruby;
                    break;
                case 2:
                    je = JewelEnum.diamond;
                    break;
                case 3:
                    je = JewelEnum.topaze;
                    break;
                case 4:
                    je = JewelEnum.emerald;
                    break;
            }

            String[] currentjewel = itemData.get(je.toString());

            this.tradeItems[i] = new Jewel(
                    currentjewel[Math.abs(currentjewel.length - nbJewel) % currentjewel.length].split(":")[0],
                    Integer.parseInt(
                            currentjewel[Math.abs(currentjewel.length - nbJewel) % currentjewel.length].split(":")[1]
                                    .trim()),
                    je);

            this.totalGold -= Integer
                    .parseInt(currentjewel[Math.abs(currentjewel.length - nbJewel) % currentjewel.length].split(":")[1]
                            .trim());

        }

        for (int nfts = 0; nfts < nbRelics; nfts++, i++) {
            String[] currentRelic = itemData.get("relic");
            this.tradeItems[i] = new Relic(
                    currentRelic[Math.abs(currentRelic.length - nfts) % currentRelic.length].split(":")[0],
                    Integer
                            .parseInt(currentRelic[Math.abs(currentRelic.length - nfts) % currentRelic.length]
                                    .split(":")[1].trim()));
            this.totalGold -= Integer
                    .parseInt(currentRelic[Math.abs(currentRelic.length - nfts) % currentRelic.length].split(":")[1]
                            .trim());

        }

        this.nbOfTradeitems += nbJewel + nbScrolls + nbRelics;
    }

    /**
     * Initialise this.nonplayer with the number given of eachtype of NPC. For each
     * trader it will share evenly trading items
     * 
     * @param nbExplorerCrazy the numbers of crazy explorer for the maze
     * @param nbExplorerSafe  the numbers of safe explorer for the maze
     * @param nbSphynx        the numbers of sphynx for the maze
     * @param nbTraders       the numbers of traders for the maze
     */
    public void createNPC() {
        Random rng = new Random();

        int nbExplorerCrazy = (int) 50;
        int nbExplorerSafe = (int) 50;
        int nbSphynx = (int) 50;
        int nbTraders = (int) 50;

        GameDataReader text = new GameDataReader();

        text.setData("TraderNames.txt");
        String[] traderNames = text.getData();

        text.setData("ExplorerNames.txt");
        String[] explorerNames = text.getData();

        text.setData("SphynxNames.txt");

        String[] sphynxNames = text.getData();

        text.setData("Enigmas.txt");
        String[] sentences = text.getData();
        String[][] enigmas = new String[sentences.length][];
        for (int i = 0; i < sentences.length; i++) {
            enigmas[i] = sentences[i].split("-");
        }

        Action explorerAction[] = new Action[2];
        Action tradeAction[] = new Action[4];
        Action sphynxAction[] = new Action[3];

        explorerAction[0] = new MoveNPC(this.theMaze);
        explorerAction[1] = new ExplorerTalk(this.theMaze);

        sphynxAction[1] = new RiddleWith(this.theMaze);

        tradeAction[0] = new MoveNPC(this.theMaze);
        tradeAction[1] = new TradeWith(theMaze);
        tradeAction[2] = new SellMenu(theMaze);
        tradeAction[3] = new BuyMenu(theMaze);

        int i = 0;

        for (i = 0; i < nbExplorerCrazy; i++) {
            String name = explorerNames[i] + " the Explorer";
            Explorer toAdd = new Explorer(name, this.theMaze.getRandomCell(), explorerAction, true);
            toAdd.setClue(this.objective.giveFakeRandomClue());
            this.nonplayers[i] = toAdd;

        }
        for (i = 0; i < nbExplorerSafe; i++) {
            String name = explorerNames[i] + " the Exp1orer";

            Explorer toAdd = new Explorer(name, this.theMaze.getRandomCell(), explorerAction, true);
            toAdd.setClue(this.objective.giveRandomClue());
            this.nonplayers[i + nbExplorerCrazy] = toAdd;
        }
        for (i = 0; i < nbSphynx; i++) {
            String name = sphynxNames[i] + " the Sphynx";
            this.nonplayers[i + nbExplorerCrazy + nbExplorerSafe] = new Sphynx(name, this.theMaze.getRandomCell(),
                    sphynxAction,
                    enigmas[i % enigmas.length],
                    new Parchment("Parchment of " + name, 500, this.objective.giveRandomClue()));

        }
        for (i = 0; i < nbTraders; i++) {
            String name = traderNames[i] + " the Trader";

            int itemIndex = 0;
            Item[] add = new Item[10];
            for (Item it : this.getSomeTradeItems(2)) {
                add[itemIndex] = it;
                itemIndex++;
            }

            this.nonplayers[i + nbExplorerCrazy + nbExplorerSafe + nbSphynx] = new Trader(name,
                    this.theMaze.getRandomCell(),
                    tradeAction, rng.nextInt(8000) + 1500, add);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void main(String[] args) {

        // pour le nom du joueur et le lore
        // System.out.println(args[0]);

        String algoPourMaze = null;
        if (args.length < 0) {
            algoPourMaze = args[0];

        }

        String s = "Player";
        GameDataReader lore = new GameDataReader("\n");
        Scanner scanIn = new Scanner(System.in);

        System.out.println("\t ~~Press Enter to continue~~");
        scanIn.nextLine();

        lore.setData("openingLore.txt");
        for (String str : lore.getNormalData()) {
            System.out.println(str.replaceAll("#insertname#", s));
            if (str.contains("Your name is :")) {
                s = scanIn.nextLine();
                if (s.length() < 1) {
                    s = "Adventurer";
                }
            } else {
                scanIn.nextLine();

            }

        }
        Maze theMaze;
        Random rngalea = new Random();

        /*
         * if (algoPourMaze == null) {
         * int alemaze = rngalea.nextInt(2);
         * 
         * switch (alemaze) {
         * case 0:
         * algoPourMaze = "division";
         * break;
         * case 1:
         * algoPourMaze = "recursif";
         * break;
         * }
         * 
         * }
         */

        if (algoPourMaze == "recursif") {
            theMaze = new Maze(10, 10, true);
            MazeRecursif algor = new MazeRecursif(theMaze);
            theMaze.initBoard(algor);
        } else if (algoPourMaze == "division") {
            theMaze = new Maze(10, 10, false);

            MazeDivision algod = new MazeDivision(theMaze);
            theMaze.initBoard(algod);

        } else {
            theMaze = new Maze(10, 10, false);
        }

        // CREATION DES ACTIONS DU HEROS
        Action[] heroActions = new Action[5];
        HeroTurn playTurn = new HeroTurn(theMaze, "Jouer un tour");
        LookInventory lookInventory = new LookInventory(theMaze);
        heroActions[0] = playTurn;
        heroActions[1] = lookInventory;

        // CREATION DU HEROS
        Hero player = new Hero(s,
                theMaze.getCell(rngalea.nextInt(theMaze.getHeight()), rngalea.nextInt(theMaze.getHeight())),
                heroActions, 500,
                new Item[16]);

        // CREATION DU GAME
        jeu gaming = new jeu(theMaze, player);

        // CREATION ITEM ET PLACEMENT
        /*
         * int itemsnbs[] = new int[3];
         * int npcnbs[] = new int[3];
         * 
         * for(int nb=0; nb <3;nb++){
         * itemsnbs[nb] = rngalea.nextInt(theMaze.getHeight()/2);
         * npcnbs[nb] = rngalea.nextInt(theMaze.getHeight()/2);
         * 
         * }
         */
        gaming.createTradeItems();
        gaming.createGroundItems();
        gaming.randomizeItems();

        // CREATION DES NPC ET PLACEMENT2
        gaming.createNPC();
        gaming.randomizeNPC();

        // LANCER LE JEU
        gaming.startPlaying();

        lore.setData("endingLore.txt");
        for (String str : lore.getNormalData()) {
            System.out.println(str.replaceAll("#insertname#", s));
            scanIn.nextLine();
        }

    }
}