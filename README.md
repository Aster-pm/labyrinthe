# l2s4-projet-2022

# Equipe

- Nassim GUINET
- Flavien THIBAUT
- Yanis HADID
- Etienne RENOULT

# Sujet

[Le sujet 2022](https://www.fil.univ-lille1.fr/portail/index.php?dipl=L&sem=S4&ue=Projet&label=Documents)

# Executer le Projet

Une fois le terminal ouvert, il vous suffit de faire un "make", cette commande va vous générer la javadoc, les classes et va lancer le jeu.

# Livrables

## Livrable 1

### Atteinte des objectifs
L'algorithme récursif pour la conception du labyrinthe est fonctionnel, ainsi que son affichage. Les doctest pour la classe Cell est faite aussi. Cependant il manque un autre algorithme que nous n'avons pas encore réussi à implémenter, ainsi que les doctest pour les classes de Maze à écrire.

### Difficultés restant à résoudre
On a commencé trop vite à faire les objets, personnages et autres classes du jeu au lieu de se concentrer sur le labyrinthe, il faut donc d'abord se dépêcher de régler cette question et seulement après on pourra travailler et apporter des corrections sur le reste.


## Livrable 2

### Atteinte des objectifs
Nous avons un affichage de labyrinthe fonctionnel, une système de déplacement fonctionnel, nous pouvons intéragir avec les cellules pour voir ce qu'il y a dedans. Nous avons des NPC modélisé (fou, altruiste, sphynx, marchand) mais nous ne pouvons pas encore vraiment intéragir avec (ils disent juste bonjour). Nous avons aussi des items mais impossible d'intéragir avec pour le moment.

### Difficultés restant à résoudre
Comment travailler autour des objectif, indices et enigmes. Optimiser et nettoyer le code.

### Executer le projet
Vous pouvez executer la commande "make comp" pour compiler les .java puis executer "make exe" pour executer un exemple du fonctionnement du jeu (comme expliquer plus haut, l'intéraction avec les objets ne fonctionne pas, déclenche une erreur quand on les ramassent).

## Livrable 3

### Atteinte des objectifs
Le héro est capable de faire toute les actions, il peut se déplacer, parler aux NPC, ramasser les objets dans les cases, inspecter les objet et regarder son inventaire. Les actions des NPC ne sont pas toutes finis mais le Trader est fini, il peut vendre des objets et aussi les achetés. Le jeu est totalement jouable si l'ont ne mets que des trader pour les NPC, il n'y a pas d'erreur sur les objets et le hero.

### Difficultés restant à résoudre
Il nous reste le reste des actions pour les NPC et tout les tests

### Executer le projet
Vous pouvez executer la commande "make comp" pour compiler les .java puis executer "make exe" pour executer un exemple du fonctionnement du jeu (avec comme expliquer plus haut que des Trader pour les NPC).

## Livrable 4

### Atteinte des objectifs

### Difficultés restant à résoudre

### Executer le projet
pour tester jeu.jar demandé dans le rendu:
make exe_jeu (sans aucun mur)
make exe_jeu_recursif
make exe_jeu_division

pour tester une générationde labyrinthe plus conventionnel et aléatoire :
make exe_game

# Journal de bord

## Semaine 1
Construction du diagramme UML à partir d'une première lecture du sujet. Réfléxion autour des algorithmes itératifs et récursifs, nous avons ensuite créer les classes les plus importantes (Cells,Maze,Characters,Heroes) selon ce que nous avons établi dans le diagramme. Pas de problème de rencontrés pour l'instant :thumbs_up: 
Nous envisageons pour la prochaine séance de mettre en place les algorithmes et de compléter le diagramme UML afin de trouver un accord commun et une base solide sur quoi travailler.

## Semaine 2
Ajustement du diagramme UML, Plus des classe codées(mazeAlgo,Objective,NPC). Debut d'implémentation des algorithme nécessaire pour le labyrinthe. Discussion entre nous pour essayer de trouver le moyen le plus optimisé et pratique pour réaliser le projet.

## Semaine 3
Modification de plusieurs classes comme Cell pour les adapter à nos besoins, écriture de la javadoc de Hero et Maze, écriture des classes crazy, altruistic, character et gameManager, suppresion de la classe Gold jugé inutile. L'un des algo de creation de labyrinthe est presque fini.

## Semaine 4
Ecriture des test des classes Maze, Cell, Mazerecursif. Ecriture methode equals de Maze et de Cell et ecriture de javadoc. Le premier algo MazeRecursif a été fini.

## Semaine 5
Rediscution autour de l'UML, avancée dans le deuxieme algo. Ecriture des classes Character, NPC et Item. Discution sur une possible classe Player pour représenter le joueur et possiblement stocker des scores.

## Semaine 6
Création tableau de répartition des tâches. Ecriture des classes Marchand, ItemDoesNotExist, Altruistic/Crazy, Sphynx, Item. Discutions fonctionnement des classes des npc et des indices.

## Semaine 7
Discussion sur la classe objectives et la façon de déterminer les différents objectifs selon le score,les item et les NPC et réflexion sur la façon de gérer les indices à partir de objectifs. Mise à jour des tests et touches finales pour le livrable.

## Semaine 8
Nous avons refait le code des personnages pour simplifier les actions car le code n'était pas uni et était désordonné. Nous avons aussi modifier le code des classes Game et Maze pour coller aux modifications.

## Semaine 9
Nous avons commencer les actions du Hero et nous avons modifié les classes games, maze, etc pour corriger les erreurs produites par les changements.

## Semaine 10
Test du bon fonctionnement du jeu et de l'interface, modification de l'affichage du labyrinthe, ecriture de doc et d'actions pour le hero et les NPC

## Semaine 11
Création des énigmes, écriture d'actions et d'objectifs et écriture "Executer le Projet" du readme

## Semaine 12
Jeu fonctionnel à 90%
labyrinthe sans murs de 10x10 avec 50 instances de Trader + 50 de Explorer etc... et objectif en [8:3] et 5 Gold : `make exe_jeu`
labyrinthe avec algorithme de division 8x8 et contenu aléatoire : `make exe`

## Bug restants
- remove un Item d'indice > 1 fait disparaître tout l'inventaire
- plusieurs NPC sur la même case empêchent de prendre le premier Item

Lien Drive vers le diapo :
<https://drive.google.com/drive/folders/1RgJTt1OzeX2ebjy4WKaAC6iWAXQoGe2W?usp=sharing>
