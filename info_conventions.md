## COULEURS
On dispose de 7 couleurs, qu'on utilisera pour les mêmes choses
RED : Pour les erreurs
YELLOW : Pour l'argent
GREEN : Pour l'inventaire du Héro
CYAN : Pour les répliques des NPC
BLUE : Pour les Items
PURPLE : Pas encore attribué
WHITE : OPour les répliques du narrateur
BLACK : On ne l'utilise pas
RESET : Couleur par défaut (à la fin de chaque String de couleur)

### Format
- tabulation avant les propositions de menu
- Surlignage + tabulation pour les fins d'actions importantes
- Titre de l'action en cours

## DISTRIBUTION DES ACTIONS

POUR LES TRADERS : 
action[0] = move
action[1] = talk
action[2] = SellMenu
action[3] = BuyMenu

POUR LES EXPLORERS :
action[0] = move
action[1] = talk

POUR LES SPHYNX :
action[0] = move
action[1] = talk

POUR LE HEROS :
action[0] = heroTurn
action[1] = lookInventory
action[2] = dropMenu