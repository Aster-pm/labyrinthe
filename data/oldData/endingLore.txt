Congratulation #insertname# you managed to escape the mysterious labyrinth safe and sound !
Even if your lacking answers on what happend here, one thing is sure, you have to find a way home.
And as you walk away from that place you try to remember how you got here in the first place.
The more you look back at it the more familiar it becomes.
...
This maze, you know it and you've already been here...
But from where ?
...
Something sowly comes to your mind.
You start to remember words from someone :
<< You are not here to receive a gift, nor have you been called here by the individual you assume. Although you have indeed been called. >> 
But who ? Where does it come from ???
<< You have all been called here. Into a labyrinth of sounds and smells, misdirection and misfortune. A labyrinth with no exit, a maze with no prize. You don't even realize that you are trapped. >>
You have no idea who told you that but you've already hearrd it a thousand times it seems
<< Your lust for blood has driven you in endless circles, chasing the cries of children in some unseen chamber, always seeming so near, yet somehow out of reach. >>
It starts to make sens now, it's not the first you've been here and
...
neither the last time...
As you realize it, you feel the ground shaking, you feel your legs failling you.
And as you close your eyes you finally understand.
...
...
#insertname#
you are Doomed.